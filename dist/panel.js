import Name from '@egml/utils/Name.js';
import Widget from '@egml/utils/Widget.js';

var name = new Name('panel');

export default function Panel() {
	Widget.call(this, ...arguments);
}
Object.defineProperty(Panel, 'name', {
	value: new Name('Panel', false),
});

Panel.prototype = Object.create(Widget.prototype);
Object.defineProperty(Panel.prototype, 'constructor', {
	value: Panel,
	enumerable: false,
	writable: true,
});

Panel.prototype.name = name;
Panel.prototype.shown = false;

//	 ██████  ██████  ███    ██ ███████ ████████ ██████  ██    ██  ██████ ████████
//	██      ██    ██ ████   ██ ██         ██    ██   ██ ██    ██ ██         ██
//	██      ██    ██ ██ ██  ██ ███████    ██    ██████  ██    ██ ██         ██
//	██      ██    ██ ██  ██ ██      ██    ██    ██   ██ ██    ██ ██         ██
//	 ██████  ██████  ██   ████ ███████    ██    ██   ██  ██████   ██████    ██
// 
//	#construct
Panel.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	this.saveInstanceOf(Panel);
};

//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
// 
//	#initialize
Panel.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	if (!this.dom.self) {
		throw new Error(`Не найдены элементы в DOM для виджета типа "${this.constructor.name}" с ID "${this.id[this.constructor.name.camel()]}"`);
	}
	this.setDatasetIdOf(Panel);
	
	this.dom.self.style.display = null;
	this.show();
};

Panel.prototype.show = function()
{
	if (this.shown == false) {
		this.dom.self.classList.remove(this.name.cssMod('hidden'));
		this.shown = true;
	}
};

Panel.prototype.hide = function()
{
	if (this.shown == true) {
		this.dom.self.classList.add(this.name.cssMod('hidden'));
		this.shown = false;
	}
};

Panel.prototype.toggle = function()
{
	if (this.shown == true) {
		this.hide();
	} else {
		this.show();
	}
	return this.shown;
};