var Panel = (function () {
	'use strict';

	

	function ___$insertStyle(css) {
	  if (!css) {
	    return;
	  }
	  if (typeof window === 'undefined') {
	    return;
	  }

	  var style = document.createElement('style');

	  style.setAttribute('type', 'text/css');
	  style.innerHTML = css;
	  document.head.appendChild(style);
	  return css;
	}

	//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████

	//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
	//  ██       ██ ██     ██    ██      ████   ██ ██   ██
	//  █████     ███      ██    █████   ██ ██  ██ ██   ██
	//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
	//  ███████ ██   ██    ██    ███████ ██   ████ ██████
	//
	// #extend
	function extend(dest, source) {
		for (var prop in source) {
			dest[prop] = source[prop];
		}
		return dest;
	}

	//  ██ ███████
	//  ██ ██
	//  ██ ███████
	//  ██      ██
	//  ██ ███████
	// 
	//   ██████  ██████       ██ ███████  ██████ ████████
	//  ██    ██ ██   ██      ██ ██      ██         ██
	//  ██    ██ ██████       ██ █████   ██         ██
	//  ██    ██ ██   ██ ██   ██ ██      ██         ██
	//   ██████  ██████   █████  ███████  ██████    ██
	//
	// #is #object
	// Сперто из interact.js (сперто немного, но все равно: http://interactjs.io/)
	function isObject(thing) {
		return !!thing && (typeof thing === 'object');
	}

	//  ███████ ████████ ██████  ██ ███    ██  ██████
	//  ██         ██    ██   ██ ██ ████   ██ ██
	//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
	//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
	//  ███████    ██    ██   ██ ██ ██   ████  ██████
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #string #case
	function stringCase(bits, type) {
		if (!bits || !bits.length) {
			return '';
		}
		if (typeof bits == 'string') {
			bits = [ bits ];
		}
		var string =  '';
		switch (type) {
			case 'camel':
				bits.forEach(function(bit, i) {
					if (i == 0) {
						string += firstLowerCase(bit);
					} else {
						string += firstUpperCase(bit);
					}
				});
				break;

			case 'capitalCamel':
				bits.forEach(function(bit) {
					string += firstUpperCase(bit);
				});
				break;

			case 'flat':
				string = bits.join('');
				break;

			case 'snake':
				string = bits.join('_');
				break;

			case 'kebab':
				string = bits.join('-');
				break;

			case 'dot':
				string = bits.join('.');
				break;
		
			case 'asIs':
			default:
				string = bits.join('');
				break;
		}
		return string;
	}

	//  ███████ ██ ██████  ███████ ████████
	//  ██      ██ ██   ██ ██         ██
	//  █████   ██ ██████  ███████    ██
	//  ██      ██ ██   ██      ██    ██
	//  ██      ██ ██   ██ ███████    ██
	// 
	//  ██    ██ ██████  ██████  ███████ ██████      ██
	//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
	//  ██    ██ ██████  ██████  █████   ██████    ██
	//  ██    ██ ██      ██      ██      ██   ██  ██
	//   ██████  ██      ██      ███████ ██   ██ ██
	// 
	//  ██       ██████  ██     ██ ███████ ██████
	//  ██      ██    ██ ██     ██ ██      ██   ██
	//  ██      ██    ██ ██  █  ██ █████   ██████
	//  ██      ██    ██ ██ ███ ██ ██      ██   ██
	//  ███████  ██████   ███ ███  ███████ ██   ██
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #first #upper #lower #case
	function firstUpperCase(string) {
		return string[0].toUpperCase() + string.slice(1);
	}function firstLowerCase(string) {
		return string[0].toLowerCase() + string.slice(1);
	}
	//	██ ███    ██ ███████ ███████ ██████  ████████
	//	██ ████   ██ ██      ██      ██   ██    ██
	//	██ ██ ██  ██ ███████ █████   ██████     ██
	//	██ ██  ██ ██      ██ ██      ██   ██    ██
	//	██ ██   ████ ███████ ███████ ██   ██    ██
	// 
	//	███████ ██    ██  ██████
	//	██      ██    ██ ██
	//	███████ ██    ██ ██   ███
	//	     ██  ██  ██  ██    ██
	//	███████   ████    ██████
	// 
	//	███████ ██    ██ ███    ███ ██████   ██████  ██
	//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
	//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
	//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
	//	███████    ██    ██      ██ ██████   ██████  ███████
	// 
	//	#insert #svg #symbol
	function insertSvgSymbol() {
		// -------------------------------------------
		//  Вариант с отдельным родительским
		// 	SVG-элементом для каждого символа:
		// -------------------------------------------
		var options = {
			symbol: null,
			mountElement: document.body,
			containerClass: null,
		};
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.symbol = arguments[0];
			options.mountElement = arguments[1] || options.mountElement;
			options.containerClass = arguments[2] || options.containerClass;
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var parser = new DOMParser();
		var symbolDocument = parser.parseFromString(
			`<svg xmlns="http://www.w3.org/2000/svg" 
			${ options.containerClass ? `class="${options.containerClass}"` : '' } 
			style="display: none;">
			${options.symbol}
		</svg>`,
			'image/svg+xml'
		);
		var symbolNode = document.importNode(symbolDocument.documentElement, true);
		options.mountElement.appendChild(symbolNode);

		// -------------------------------------------
		//  Вариант со вставкой в один родительский 
		// 	SVG-элемент:
		// -------------------------------------------
		// var options = {
		// 	symbol: null,
		// 	containerId: 'svg_sprite',
		// 	mountElement: document.body,
		// };
		 
		// if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
		// 	options.symbol = arguments[0];
		// 	options.containerId = arguments[1] || options.containerId;
		// 	options.mountElement = arguments[2] || options.mountElement;
		// } else if (!!arguments[0] && typeof arguments[0] == 'object') {
		// 	extend(options, arguments[0]);
		// } else {
		// 	return;
		// }

		// var container = document.getElementById(options.containerId);
		
		// if (container == null) {
		// 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		// 	container.setAttribute('id', options.containerId);
		// 	container.style.cssText = 'display: none;';
		// 	options.mountElement.appendChild(container);
		// }

		// var parser = new DOMParser();
		// var symbolDocument = parser.parseFromString(
		// 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		// 		${options.symbol}
		// 	</svg>`,
		// 	'image/svg+xml'
		// );
		// var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
		// container.appendChild(symbolNode);
	}

	function StringCase(bits) {
		this.bits = bits;

		if (Array.isArray(bits)) {
			this.asIs = this.bits.join('');
			bits.forEach(function(bit, i) {
				bits[i] = bit.toLowerCase();
			});
		} else {
			this.asIs = this.bits;
			bits = bits.toLowerCase();
		}

		this.camel = stringCase(bits, 'camel');
		this.capitalCamel = stringCase(bits, 'capitalCamel');
		this.kebab = stringCase(bits, 'kebab');
		this.snake = stringCase(bits, 'snake');
		this.flat = stringCase(bits, 'flat');
		this.dot = stringCase(bits, 'dot');
	}
	StringCase.prototype.toString = function() {
		return this.asIs;
	};

	function Name(nameBits, nsBits) {
		var bits;

		// Далее идет обработка параметра nsBits:
		// - если он не задан, пропустить все и взять ns из прототипа
		// - если он boolean и true, то также пропустить и использовать ns 
		//   из прототипа
		// - если он boolean и false, то nsBits - пустая строка
		// - и если в результате nsBits строка или массив, то создать новое 
		//   свойство ns поверх геттера прототипа со значением nsBits
		if (nsBits != null) {
			if (typeof nsBits == 'boolean' && nsBits == false) {
				nsBits = '';
			}
			if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
				Object.defineProperty(this, 'ns', {
					value: new StringCase(nsBits),
					writable: true,
					configurable: true,
					enumerable: true,
				});
			}
		}

		var hasNs = this.ns && this.ns.toString() != '';

		this.base = new StringCase(nameBits);

		// #asIs #flat #camel #capital #kebab
		var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];
		for (var i = 0; i < types.length; i++) {
			if (hasNs) {
				this['_' + types[i]] = stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
			} else {
				this['_' + types[i]] = this.base[types[i]];
			}
			this[types[i]] = (function(type, nameObj) {
				return function(suffix) {
					if (suffix) {
						if (type != 'asIs') {
							if (Array.isArray(suffix)) {
								suffix.forEach(function(bit, i) {
									suffix[i] = bit.toLowerCase();
								});
							}
						}
						return stringCase([nameObj['_' + type], stringCase(suffix, type)], type);
					} else {
						return nameObj['_' + type];
					}
				};
			})(types[i], this);
		}

		//  ██████   ██████  ████████
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██████   ██████     ██
		//
		// #dot
		var bits = [ this.base.camel ];
		if (hasNs) {
			bits.unshift(this.ns.camel);
		}
		this._dot = stringCase(bits, 'dot');
		this.dot = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._dot, stringCase(suffix, 'camel')], 'dot');
			} else {
				return this._dot;
			}
		};

		//  ███████ ██    ██ ███████ ███    ██ ████████
		//  ██      ██    ██ ██      ████   ██    ██
		//  █████   ██    ██ █████   ██ ██  ██    ██
		//  ██       ██  ██  ██      ██  ██ ██    ██
		//  ███████   ████   ███████ ██   ████    ██
		//
		// #event
		this.event = this.dot;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		//
		// #css
		bits = [ this.base.snake ];
		if (hasNs) {
			bits.unshift(this.ns.snake);
		}
		this._css = stringCase(bits, 'kebab');
		this.css = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._css, stringCase(suffix, 'snake')], 'kebab');
			} else {
				return this._css;
			}
		};

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//  ███    ███  ██████  ██████
		//  ████  ████ ██    ██ ██   ██
		//  ██ ████ ██ ██    ██ ██   ██
		//  ██  ██  ██ ██    ██ ██   ██
		//  ██      ██  ██████  ██████
		//
		// #css #modificator
		this.cssModificator = function(modificatorName) {
			if (modificatorName != null) {
				if (Array.isArray(modificatorName)) {
					modificatorName.forEach(function(bit, i) {
						modificatorName[i] = bit.toLowerCase();
					});
				}
				return this.css() + '--' + stringCase(modificatorName, 'snake');
			} else {
				return this.css();
			}
		};
		this.cssMod = this.cssModificator;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//   ██████ ██       █████  ███████ ███████
		//  ██      ██      ██   ██ ██      ██
		//  ██      ██      ███████ ███████ ███████
		//  ██      ██      ██   ██      ██      ██
		//   ██████ ███████ ██   ██ ███████ ███████
		// 
		//  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
		//  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
		//       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
		//
		// #css #class #selector
		this.cssClassSelector = function(appendage) {
			return '.' + this.css(appendage);
		};
		this.selector = function(appendage) {
			return this.cssClassSelector(appendage);
		};
	}
	var ns;
	Object.defineProperty(Name.prototype, 'ns', {
		set: function(value) {
			ns = new StringCase(value);
		},
		get: function() {
			return ns;
		},
	});
	Name.prototype.toString = function() {
		return this._asIs;
	};

	___$insertStyle("/*! normalize-scss | MIT/GPLv2 License | bit.ly/normalize-scss */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */\n}\n\n/* Sections\n   ========================================================================== */\n/**\n * Remove the margin in all browsers (opinionated).\n */\nbody {\n  margin: 0;\n}\n\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block;\n}\n\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0;\n}\n\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\nfigcaption,\nfigure {\n  display: block;\n}\n\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px;\n}\n\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */\n}\n\n/**\n * Add the correct display in IE.\n */\nmain {\n  display: block;\n}\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n/* Links\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */\n}\n\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  text-decoration: underline dotted;\n  /* 2 */\n}\n\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit;\n}\n\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder;\n}\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic;\n}\n\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000;\n}\n\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%;\n}\n\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block;\n}\n\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0;\n}\n\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none;\n}\n\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden;\n}\n\n/* Forms\n   ========================================================================== */\n/**\n * 1. Change the font styles in all browsers (opinionated).\n * 2. Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: sans-serif;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */\n}\n\n/**\n * Show the overflow in IE.\n */\nbutton {\n  overflow: visible;\n}\n\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none;\n}\n\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=button],\n[type=reset],\n[type=submit] {\n  -webkit-appearance: button;\n  /* 2 */\n}\n\nbutton,\n[type=button],\n[type=reset],\n[type=submit] {\n  /**\n   * Remove the inner border and padding in Firefox.\n   */\n  /**\n   * Restore the focus styles unset by the previous rule.\n   */\n}\nbutton::-moz-focus-inner,\n[type=button]::-moz-focus-inner,\n[type=reset]::-moz-focus-inner,\n[type=submit]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\nbutton:-moz-focusring,\n[type=button]:-moz-focusring,\n[type=reset]:-moz-focusring,\n[type=submit]:-moz-focusring {\n  outline: 1px dotted ButtonText;\n}\n\n/**\n * Show the overflow in Edge.\n */\ninput {\n  overflow: visible;\n}\n\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=checkbox],\n[type=radio] {\n  box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */\n}\n\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=number]::-webkit-inner-spin-button,\n[type=number]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=search] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */\n  /**\n   * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n   */\n}\n[type=search]::-webkit-search-cancel-button, [type=search]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */\n}\n\n/**\n * Correct the padding in Firefox.\n */\nfieldset {\n  padding: 0.35em 0.75em 0.625em;\n}\n\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  box-sizing: border-box;\n  /* 1 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  color: inherit;\n  /* 2 */\n  white-space: normal;\n  /* 1 */\n}\n\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */\n}\n\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto;\n}\n\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in Edge, IE, and Firefox.\n */\ndetails {\n  display: block;\n}\n\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item;\n}\n\n/*\n * Add the correct display in IE 9-.\n */\nmenu {\n  display: block;\n}\n\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block;\n}\n\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none;\n}\n\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none;\n}\n\n.text.text--normal, body, .panel-button {\n  font-size: 1em;\n  font-weight: 400;\n  font-family: sans-serif;\n  font-style: normal;\n  line-height: 1.3;\n  color: #000;\n  text-align: left;\n  text-decoration: none;\n  text-transform: none;\n}\n\n.text.text--muted {\n  color: #999;\n}\n\nstrong, .text.text--bold {\n  font-weight: 700;\n}\n\n.text.text--normal, body {\n  font-size: 1.4rem;\n}\n\n.text.text--size-10 {\n  font-size: 1rem;\n}\n\n.text.text--size-relative-10 {\n  font-size: 0.7142857143em;\n}\n\n.text.text--size-12 {\n  font-size: 1.2rem;\n}\n\n.text.text--size-relative-12 {\n  font-size: 0.8571428571em;\n}\n\n.text.text--size-16 {\n  font-size: 1.6rem;\n}\n\n.text.text--size-relative-16 {\n  font-size: 1.1428571429em;\n}\n\n.text.text--size-20 {\n  font-size: 2rem;\n}\n\n.text.text--size-relative-20 {\n  font-size: 1.4285714286em;\n}\n\n.text.text--size-24 {\n  font-size: 2.4rem;\n}\n\n.text.text--size-relative-24 {\n  font-size: 1.7142857143em;\n}\n\n.text.text--center {\n  text-align: center;\n}\n\n.text.text--middle {\n  vertical-align: middle;\n}\n\n.text.text--no-wrap {\n  white-space: nowrap;\n}\n\n.text.text--green {\n  color: green;\n}\n\n.row-item.row-item--margin-30:first-child, .row.row--item-margin-30 > .row-item:first-child, .row-item.row-item--margin-20:first-child, .row.row--item-margin-20 > .row-item:first-child, .row-item.row-item--margin-15:first-child, .row.row--item-margin-15 > .row-item:first-child, .row-item.row-item--margin-10:first-child, .row.row--item-margin-10 > .row-item:first-child, .row-item.row-item--margin-5:first-child, .row.row--item-margin-5 > .row-item:first-child {\n  margin-left: 0;\n}\n.row-item.row-item--margin-30:last-child, .row.row--item-margin-30 > .row-item:last-child, .row-item.row-item--margin-20:last-child, .row.row--item-margin-20 > .row-item:last-child, .row-item.row-item--margin-15:last-child, .row.row--item-margin-15 > .row-item:last-child, .row-item.row-item--margin-10:last-child, .row.row--item-margin-10 > .row-item:last-child, .row-item.row-item--margin-5:last-child, .row.row--item-margin-5 > .row-item:last-child {\n  margin-right: 0;\n}\n\np.paragraph--edge:first-child, .paragraph.paragraph--edge:first-child, .row.row--edge:first-child, .panel-button:first-child {\n  margin-top: 0;\n}\np.paragraph--edge:last-child, .paragraph.paragraph--edge:last-child, .row.row--edge:last-child, .panel-button:last-child {\n  margin-bottom: 0;\n}\n\n.row-item.row-item--margin-5, .row.row--item-margin-30 > .row-item.row-item--margin-5, .row.row--item-margin-20 > .row-item.row-item--margin-5, .row.row--item-margin-15 > .row-item.row-item--margin-5, .row.row--item-margin-10 > .row-item.row-item--margin-5, .row.row--item-margin-5 > .row-item.row-item--margin-5, .row.row--item-margin-5 > .row-item {\n  margin-right: 0.5rem;\n}\n\n.row.row--margin-5 {\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n\n.row-item.row-item--margin-10, .row.row--item-margin-30 > .row-item.row-item--margin-10, .row.row--item-margin-20 > .row-item.row-item--margin-10, .row.row--item-margin-15 > .row-item.row-item--margin-10, .row.row--item-margin-10 > .row-item.row-item--margin-10, .row.row--item-margin-10 > .row-item, .row.row--item-margin-5 > .row-item.row-item--margin-10 {\n  margin-right: 1rem;\n}\n\n.row.row--margin-10 {\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n\np.paragraph--margin-15r, .paragraph.paragraph--margin-15r {\n  margin-top: 0.9375em;\n  margin-bottom: 0.9375em;\n}\n\n.row-item.row-item--margin-15, .row.row--item-margin-30 > .row-item.row-item--margin-15, .row.row--item-margin-20 > .row-item.row-item--margin-15, .row.row--item-margin-15 > .row-item.row-item--margin-15, .row.row--item-margin-15 > .row-item, .row.row--item-margin-10 > .row-item.row-item--margin-15, .row.row--item-margin-5 > .row-item.row-item--margin-15 {\n  margin-right: 1.5rem;\n}\n\np.paragraph--margin-15, .paragraph.paragraph--margin-15, .row.row--margin-15 {\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n\n.row-item.row-item--margin-20, .row.row--item-margin-30 > .row-item.row-item--margin-20, .row.row--item-margin-20 > .row-item.row-item--margin-20, .row.row--item-margin-20 > .row-item, .row.row--item-margin-15 > .row-item.row-item--margin-20, .row.row--item-margin-10 > .row-item.row-item--margin-20, .row.row--item-margin-5 > .row-item.row-item--margin-20 {\n  margin-right: 2rem;\n}\n\n.row.row--margin-20 {\n  margin-top: 2rem;\n  margin-bottom: 2rem;\n}\n\n.row.row--margin-25 {\n  margin-top: 2.5rem;\n  margin-bottom: 2.5rem;\n}\n\np.paragraph--margin-30r, .paragraph.paragraph--margin-30r, .row.row--margin, .row.row--paragraph, p, .paragraph {\n  margin-top: 1.875em;\n  margin-bottom: 1.875em;\n}\n\n.row-item.row-item--margin-30, .row.row--item-margin-30 > .row-item.row-item--margin-30, .row.row--item-margin-30 > .row-item, .row.row--item-margin-20 > .row-item.row-item--margin-30, .row.row--item-margin-15 > .row-item.row-item--margin-30, .row.row--item-margin-10 > .row-item.row-item--margin-30, .row.row--item-margin-5 > .row-item.row-item--margin-30 {\n  margin-right: 3rem;\n}\n\np.paragraph--margin-30, .paragraph.paragraph--margin-30, .row.row--margin-30 {\n  margin-top: 3rem;\n  margin-bottom: 3rem;\n}\n\n.row.row--margin-35 {\n  margin-top: 3.5rem;\n  margin-bottom: 3.5rem;\n}\n\n.row.row--margin-40 {\n  margin-top: 4rem;\n  margin-bottom: 4rem;\n}\n\n.row.row--margin-45 {\n  margin-top: 4.5rem;\n  margin-bottom: 4.5rem;\n}\n\n.row.row--margin-50 {\n  margin-top: 5rem;\n  margin-bottom: 5rem;\n}\n\n.panel-button, .row {\n  display: flex;\n  align-items: center;\n  justify-content: normal;\n  flex-flow: row nowrap;\n}\n\n.row.row--block {\n  display: flex;\n}\n\n.row.row--inline {\n  display: inline-flex;\n}\n\n.row.row--top {\n  align-items: flex-start;\n}\n\n.row.row--stretch {\n  align-items: stretch;\n}\n\n.row.row--bottom {\n  align-items: flex-end;\n}\n\n.row.row--left {\n  justify-content: flex-start;\n}\n\n.row.row--center {\n  justify-content: safe center;\n}\n\n.row.row--right {\n  justify-content: flex-end;\n}\n\n.row.row--vertical {\n  flex-direction: column;\n}\n\n.row-item {\n  flex: 0 1 auto;\n}\n\n.row-item.row-item--fill {\n  flex-grow: 1;\n}\n\n.row-item.row-item--no-shrink {\n  flex-shrink: 0;\n}\n\n.row-item.row-item--top {\n  align-self: flex-start;\n}\n\n.row-item.row-item--middle {\n  align-self: center;\n}\n\n.row-item.row-item--stretch {\n  align-self: stretch;\n}\n\n.row-item.row-item--bottom {\n  align-self: flex-end;\n}\n\n.row-item.row-item--clip {\n  overflow: hidden;\n}\n\nhtml {\n  font-size: 0.625em;\n}\n\nsvg {\n  width: 3rem;\n  height: 3rem;\n  stroke-width: 0;\n}\nsvg use {\n  user-select: auto;\n  -moz-user-select: none;\n}\n\np.paragraph--no-margin, .paragraph.paragraph--no-margin {\n  margin: 0;\n}\n.spacer.spacer--block {\n  display: block;\n}\n.spacer.spacer--block.spacer--5 {\n  height: 0.5rem;\n}\n.spacer.spacer--block.spacer--10 {\n  height: 1rem;\n}\n.spacer.spacer--block.spacer--15 {\n  height: 1.5rem;\n}\n.spacer.spacer--block.spacer--20 {\n  height: 2rem;\n}\n.spacer.spacer--block.spacer--25 {\n  height: 2.5rem;\n}\n.spacer.spacer--block.spacer--30 {\n  height: 3rem;\n}\n.spacer.spacer--inline {\n  display: inline-block;\n}\n.spacer.spacer--inline.spacer--5 {\n  width: 0.5rem;\n}\n.spacer.spacer--inline.spacer--10 {\n  width: 1rem;\n}\n.spacer.spacer--inline.spacer--15 {\n  width: 1.5rem;\n}\n.spacer.spacer--inline.spacer--20 {\n  width: 2rem;\n}\n.spacer.spacer--inline.spacer--25 {\n  width: 2.5rem;\n}\n.spacer.spacer--inline.spacer--30 {\n  width: 3rem;\n}\n\n@keyframes slide-in {\n  from {\n    transform: translateX(-4.1875em);\n  }\n  to {\n    transform: translateX(0);\n  }\n}\n@keyframes slide-out {\n  to {\n    transform: translateX(-4.1875em);\n  }\n}\n.panel {\n  position: fixed;\n  top: 12.5em;\n  left: 0;\n  z-index: 32766;\n}\n.panel-button {\n  background: #fff;\n  border-radius: 0.375em;\n  padding: 0.59375em;\n  margin: 0.75em 0.9375em;\n  box-shadow: 0 0 0.3125em rgba(0, 0, 0, 0.35);\n  border: 0.1875em solid transparent;\n  transition: background-color 0.15s linear;\n}\n.panel-button:hover, .panel-button:active, .panel-button:focus {\n  background: dodgerblue;\n}\n.panel-button:focus {\n  outline: 0;\n}\n.panel-button:nth-child(1) {\n  animation: 0.6s ease 0.3s both slide-in;\n}\n.panel-button:nth-child(2) {\n  animation: 0.6s ease 0.5s both slide-in;\n}\n.panel-button:nth-child(3) {\n  animation: 0.6s ease 0.7s both slide-in;\n}\n.panel-button:nth-child(4) {\n  animation: 0.6s ease 0.9s both slide-in;\n}\n.panel-button:nth-child(5) {\n  animation: 0.6s ease 1.1s both slide-in;\n}\n.panel-button:nth-child(6) {\n  animation: 0.6s ease 1.3s both slide-in;\n}\n.panel-button:nth-child(7) {\n  animation: 0.6s ease 1.5s both slide-in;\n}\n.panel-button:nth-child(8) {\n  animation: 0.6s ease 1.7s both slide-in;\n}\n.panel-button:nth-child(9) {\n  animation: 0.6s ease 1.9s both slide-in;\n}\n.panel-button:nth-child(10) {\n  animation: 0.6s ease 2.1s both slide-in;\n}\n\n.panel.panel--hidden .panel-button:nth-child(1) {\n  animation: 0.6s ease 0s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(2) {\n  animation: 0.6s ease 0.2s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(3) {\n  animation: 0.6s ease 0.4s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(4) {\n  animation: 0.6s ease 0.6s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(5) {\n  animation: 0.6s ease 0.8s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(6) {\n  animation: 0.6s ease 1s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(7) {\n  animation: 0.6s ease 1.2s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(8) {\n  animation: 0.6s ease 1.4s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(9) {\n  animation: 0.6s ease 1.6s both slide-out;\n}\n.panel.panel--hidden .panel-button:nth-child(10) {\n  animation: 0.6s ease 1.8s both slide-out;\n}\n\n.panel-button-icon {\n  width: 1.375em;\n  height: 1.375em;\n  fill: #333;\n  transition: fill 0.15s linear;\n}\n\n.panel-button:hover .panel-button-icon, .panel-button:active .panel-button-icon, .panel-button:focus .panel-button-icon {\n  fill: #fff;\n}");

	var crossStroke = "<symbol id=\"egml-icons-cross-stroke\" viewBox=\"0 0 20 20\"><path d=\"M 16.589922,2.0000143 10,8.5890049 3.4120547,2.0019908 1.9999453,3.4120092 8.5898438,10.000976 1.9999219,16.587991 3.4100781,17.998009 10,11.410995 l 6.589922,6.589014 1.410156,-1.410018 -6.589922,-6.589015 6.589922,-6.5889903 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

	var cogStroke = "<symbol id=\"egml-icons-cog-stroke\" viewBox=\"0 0 100 100\"><path style=\"line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1\" d=\"M 37.273438,0 36.441406,3.5664062 33.564453,15.892578 c -1.54098,0.749627 -3.019658,1.596166 -4.439453,2.542969 L 13.554688,13.443359 0.38671875,36.746094 12.28125,47.175781 c -0.08188,0.938116 -0.220703,1.833178 -0.220703,2.822266 0,0.852635 0.11375,1.617693 0.175781,2.427734 L 0.22265625,62.955078 13.390625,86.261719 28.771484,81.332031 c 1.46408,0.998044 2.991015,1.885437 4.582032,2.669922 l 2.789062,15.335938 26.75,0.662109 3.72461,-15.970703 c 1.391862,-0.687563 2.732611,-1.449674 4.021484,-2.296875 L 85.148438,87.179688 99.441406,64.544922 87.677734,53.171875 c 0.102468,-1.050388 0.261719,-2.059463 0.261719,-3.173828 0,-0.763255 -0.09824,-1.446695 -0.148437,-2.173828 L 99.777344,37.320312 86.603516,14.017578 71.511719,18.855469 c -1.495823,-1.03917 -3.057302,-1.963266 -4.6875,-2.777344 L 64.023438,0.66210938 37.273438,0 Z m 7.099609,9.1777344 12.105469,0.3007812 2.330078,12.8203124 2.265625,0.939453 c 2.457912,1.018966 4.780734,2.401524 6.914062,4.097657 l 1.884766,1.496093 12.501953,-4.007812 5.964844,10.552734 -9.941406,8.712891 0.291015,2.365234 c 0.153094,1.241055 0.25,2.416508 0.25,3.542969 0,1.362969 -0.139368,2.785286 -0.361328,4.273437 l -0.341797,2.289063 9.6875,9.367187 -6.470703,10.25 -12.105469,-4.544922 -1.974609,1.486329 c -2.003266,1.507649 -4.134849,2.746972 -6.369141,3.666015 l -2.144531,0.88086 -3.066406,13.15625 L 43.6875,90.521484 41.365234,77.748047 39.078125,76.814453 c -2.425985,-0.989475 -4.72166,-2.331764 -6.839844,-3.982422 l -1.876953,-1.460937 -12.738281,4.083984 -5.964844,-10.554687 9.994141,-8.759766 -0.3125,-2.384766 c -0.17203,-1.314186 -0.279297,-2.56359 -0.279297,-3.757812 0,-1.309268 0.126486,-2.667492 0.330078,-4.091797 l 0.34375,-2.412109 -9.912109,-8.691407 5.96289,-10.550781 12.873047,4.126953 1.865235,-1.417968 c 2.070313,-1.573114 4.291693,-2.856839 6.628906,-3.802735 L 41.314453,22.28125 44.373047,9.1777344 Z M 50,31.158203 C 39.651378,31.15901 31.170726,39.652401 31.169922,50.001953 31.169306,60.352559 39.651236,68.844756 50,68.845703 60.349787,68.846176 68.832719,60.354149 68.832031,50.001953 68.831228,39.650772 60.349624,31.157518 50,31.158203 Z m 0,9 c 5.480182,-3.63e-4 9.831605,4.352963 9.832031,9.84375 3.65e-4,5.491333 -4.350384,9.844001 -9.832031,9.84375 -5.481074,-5.01e-4 -9.830405,-4.352278 -9.830078,-9.84375 C 40.170348,44.511169 44.52034,40.15863 50,40.158203 Z\" color=\"#000\" font-weight=\"400\" font-family=\"sans-serif\" overflow=\"visible\" stroke-width=\"9\"/></symbol>";

	var areaToggleStroke = "<symbol id=\"egml-icons-area-toggle-stroke\" viewBox=\"0 0 20 20\"><path d=\"M 1.411,19.999 0,18.588 l 2.044,-2.044 0,-14.549 14.55,0 L 18.59,0 20,1.411 Z M 4.04,3.99 4.04,14.549 14.6,3.99 Z m 7.981,13.964 -3.992,0 0,-1.994 3.991,0 0,1.994 z m 3.989,-9.974 1.995,0 0,3.99 -1.995,0 z m 0,5.985 1.995,0 0,3.989 -3.989,0 0,-1.994 1.994,0 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

	function Id() {
	}
	Id.prototype.toString = function() {
		return this.current;
	};

	/**
	 * Базовый класс для виджетов
	 * @param {Array-like} arguments ...
	 */
	function Widget() {
		this.construct(...arguments);
	}
	/**
	 * Переопределение свойства `name` конструктора нужно при сохранении экзепляра класса в прототипе
	 * (см. метод `saveInstanceOf`)
	 */
	Object.defineProperty(Widget, 'name', {
		value: new Name('Widget', false),
	});

	Widget.prototype.name = new Name('widget');
	Widget.prototype.debug = false;
	Widget.prototype.busy = false;
	Widget.prototype.initialized = false;
	Widget.prototype.autoInitialize = true;

	/**
	 * Конструирование объекта.
	 * Этот метод не задуман для переопределения или расширения (для этого см. `constructionChain`).
	 * @param {Array-like} arguments ...
	 */
	Widget.prototype.construct = function()
	{
		var target, options;
		if (arguments[0] instanceof Element) {
			target = arguments[0];
			if (!!arguments[1] && typeof arguments[1] == 'object') {
				options = arguments[1];
				if (typeof arguments[2] == 'boolean') {
					options.autoInitialize = arguments[2];
				}
			}
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			options = arguments[0];
			if (typeof arguments[1] == 'boolean') {
				options.autoInitialize = arguments[1];
			}
		}
		this.constructionChain(target, options);
		if (this.autoInitialize) {
			this.initialize(this.dom.self);
		}
	};

	/**
	 * Конструирование объекта (только относящееся к этом объекту + вызов цепочки). Тут только тот код, 
	 * который нельзя назвать инициализацией (который не должен исполняться повторно). Этот метод задуман 
	 * для расширения или переопределения дочерними классами.
	 * @param {HTMLElement} target Элемент DOM, на котором создается виджет (иногда он нужен в конструкторе)
	 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
	 */
	Widget.prototype.constructionChain = function(target, options)
	{
		this.dom = {
			self: target,
		};
		this.id = new Id;
		this.setOptions(options);
		this.saveInstanceOf(Widget);
	};

	/**
	 * Инициализация экзепляра класса: привязка к DOM, назначение прослушки событий и т.п.
	 * Этот метод не задуман для переопределения или расширения (для этого см. `initializationChain`)
	 * @param {HTMLElement|null} target Элемент DOM, который будет основным при работе виджета
	 */
	Widget.prototype.initialize = function(target)
	{
		if (typeof target == 'undefined') {
			target = this.dom.self; // При отложенной инициализации
		}
		this.initializationChain(target);
		this.initialized = true;
	};

	/**
	 * Это псевдоним для `initialize`.
	 * @param {HTMLElement|null} target Элемент DOM, который будет основным при работе виджета
	 */
	Widget.prototype.mount = function(target)
	{
		this.initialize(target);
	};

	/**
	 * Инициализация экзепляра класса (только относящаяся к этому объекту + вызов цепочки): 
	 * привязка к DOM, назначение прослушки событий и т.п. Этот метод задуман для расширения 
	 * или переопределения дочерними классами.
	 * @param {HTMLElement} target Элемент DOM, на котором создается виджет
	 */
	Widget.prototype.initializationChain = function(target)
	{
		this.dom.self = target;
	};

	Widget.prototype.setOptions = function(options)
	{
		if (isObject(options)) {
			extend(this, options);
		}
	};

	// /*
	//  ███████  █████  ██    ██ ███████
	//  ██      ██   ██ ██    ██ ██
	//  ███████ ███████ ██    ██ █████
	//       ██ ██   ██  ██  ██  ██
	//  ███████ ██   ██   ████   ███████
	// 
	//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
	//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
	//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
	//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
	//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
	// 
	//   ██████  ███████
	//  ██    ██ ██
	//  ██    ██ █████
	//  ██    ██ ██
	//   ██████  ██
	// */
	// #save #instance #of
	/**
	 * Сохранение экземпляра класса в регистре экземпляров класса этого типа
	 * @param {Function} constructor Конструктор класса, в который происходит сохранение экземпляра
	 */
	Widget.prototype.saveInstanceOf = function(constructor)
	{
		constructor = constructor || this.constructor;
		if (!constructor.prototype.hasOwnProperty('instances')) {
			(constructor.prototype.instances = []).length++; // потому что мы хотим считать id с 1, а не с 0
			// #TODO: Переделать на что-то более корректное, ведь при таком подходе с массивом его length будет выдавать результат не единицу больший, чем в действительности есть сохраненных экзепляров. Возможно лучше использовать Set или Map.
		}
		this.id.current = constructor.prototype.instances.length;
		this.id[constructor.name._camel] = this.id >> 0; // скастовать в строку, а затем в целое число
		constructor.prototype.instances[this.id] = this;
	};

	// /*
	//  ███████ ███████ ████████
	//  ██      ██         ██
	//  ███████ █████      ██
	//       ██ ██         ██
	//  ███████ ███████    ██
	// 
	//  ██████   █████  ████████  █████  ███████ ███████ ████████
	//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
	//  ██   ██ ███████    ██    ███████ ███████ █████      ██
	//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
	//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
	// 
	//  ██ ██████   ██████  ███████
	//  ██ ██   ██ ██    ██ ██
	//  ██ ██   ██ ██    ██ █████
	//  ██ ██   ██ ██    ██ ██
	//  ██ ██████   ██████  ██
	// */
	// #set #dataset #id #of
	Widget.prototype.setDatasetIdOf = function(constructor)
	{
		constructor = constructor || this.constructor;
		var id = this.id[constructor.name._camel];
		var name = constructor.prototype.name.camel('id');
		this.dom.self.dataset[name] = id;
	};

	// /*
	//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
	//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
	//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
	//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
	//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
	// 
	//  ███████ ██████   ██████  ███    ███
	//  ██      ██   ██ ██    ██ ████  ████
	//  █████   ██████  ██    ██ ██ ████ ██
	//  ██      ██   ██ ██    ██ ██  ██  ██
	//  ██      ██   ██  ██████  ██      ██
	// 
	//  ██████   █████  ████████  █████  ███████ ███████ ████████
	//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
	//  ██   ██ ███████    ██    ███████ ███████ █████      ██
	//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
	//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
	// */
	// #instance #from #dataset
	Widget.prototype.instanceFromDataset = function(element, constructor)
	{
		return constructor.prototype.instances[element.dataset[constructor.prototype.name.camel('id')]];
	};

	Widget.prototype.setState = function(state)
	{
		var methodName = 'setState' + firstUpperCase(state);
		if (typeof this[methodName] == 'function') {
			this[methodName]();
		}
	};

	Widget.prototype.clearState = function(state)
	{
		if (state == 'all') {
			// обход всех
			for (var property in this) {
				if (property.match(/^clearState(?!All).+/)) {
					this[property]();
				}
			}
		} else {
			if (!state) {
				if (!this.state) return;
				state = this.state;
			}
			var methodName = 'clearState' + firstUpperCase(state);
			if (typeof this[methodName] == 'function') {
				this[methodName]();
			}
		}
	};

	var name = new Name('panel');

	function Panel() {
		Widget.call(this, ...arguments);
	}
	Object.defineProperty(Panel, 'name', {
		value: new Name('Panel', false),
	});

	Panel.prototype = Object.create(Widget.prototype);
	Object.defineProperty(Panel.prototype, 'constructor', {
		value: Panel,
		enumerable: false,
		writable: true,
	});

	Panel.prototype.name = name;
	Panel.prototype.shown = false;

	//	 ██████  ██████  ███    ██ ███████ ████████ ██████  ██    ██  ██████ ████████
	//	██      ██    ██ ████   ██ ██         ██    ██   ██ ██    ██ ██         ██
	//	██      ██    ██ ██ ██  ██ ███████    ██    ██████  ██    ██ ██         ██
	//	██      ██    ██ ██  ██ ██      ██    ██    ██   ██ ██    ██ ██         ██
	//	 ██████  ██████  ██   ████ ███████    ██    ██   ██  ██████   ██████    ██
	// 
	//	#construct
	Panel.prototype.constructionChain = function(target, options)
	{
		Widget.prototype.constructionChain.call(this, target, options);
		this.saveInstanceOf(Panel);
	};

	//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
	//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
	//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
	//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
	//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
	// 
	//	#initialize
	Panel.prototype.initializationChain = function(target)
	{
		Widget.prototype.initializationChain.call(this, target);
		if (!this.dom.self) {
			throw new Error(`Не найдены элементы в DOM для виджета типа "${this.constructor.name}" с ID "${this.id[this.constructor.name.camel()]}"`);
		}
		this.setDatasetIdOf(Panel);
		
		this.dom.self.style.display = null;
		this.show();
	};

	Panel.prototype.show = function()
	{
		if (this.shown == false) {
			this.dom.self.classList.remove(this.name.cssMod('hidden'));
			this.shown = true;
		}
	};

	Panel.prototype.hide = function()
	{
		if (this.shown == true) {
			this.dom.self.classList.add(this.name.cssMod('hidden'));
			this.shown = false;
		}
	};

	Panel.prototype.toggle = function()
	{
		if (this.shown == true) {
			this.hide();
		} else {
			this.show();
		}
		return this.shown;
	};

	var name$1 = new Name('panel');

	document.addEventListener('DOMContentLoaded', function() {

		insertSvgSymbol(cogStroke);
		insertSvgSymbol(crossStroke);
		insertSvgSymbol(areaToggleStroke);

		// // Как будто это возвращается от сервера
		// insertHtml(mainTemplate.render({
		// 	id: 1,
		// 	name: name.css(),
		// 	scale: 1, // #TODO(1)
		// 	dockable: false,
		// }));

	});

	return Panel;

}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVpbGQuanMiLCJzb3VyY2VzIjpbIi4uL25vZGVfbW9kdWxlcy9AZWdtbC91dGlscy91dGlscy5qcyIsIi4uL25vZGVfbW9kdWxlcy9AZWdtbC91dGlscy9TdHJpbmdDYXNlLmpzIiwiLi4vbm9kZV9tb2R1bGVzL0BlZ21sL3V0aWxzL05hbWUuanMiLCIuLi9ub2RlX21vZHVsZXMvQGVnbWwvaWNvbnMvZGlzdC9lc20vY3Jvc3Mtc3Ryb2tlLmpzIiwiLi4vbm9kZV9tb2R1bGVzL0BlZ21sL2ljb25zL2Rpc3QvZXNtL2NvZy1zdHJva2UuanMiLCIuLi9ub2RlX21vZHVsZXMvQGVnbWwvaWNvbnMvZGlzdC9lc20vYXJlYS10b2dnbGUtc3Ryb2tlLmpzIiwiLi4vbm9kZV9tb2R1bGVzL0BlZ21sL3V0aWxzL0lkLmpzIiwiLi4vbm9kZV9tb2R1bGVzL0BlZ21sL3V0aWxzL1dpZGdldC5qcyIsIi4uL2Rpc3QvcGFuZWwuanMiLCJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojiloggICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDiloTiloQg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAgICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICAgICAgICAgICAgICAgICAgICAgIOKWgOKWgFxyXG4vLyAjcmVxdWVzdFxyXG5leHBvcnQgZnVuY3Rpb24gcmVxdWVzdChvcHRpb25zKSB7XHJcblx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cdG9wdGlvbnMudHlwZSA9IG9wdGlvbnMudHlwZSB8fCAnZ2V0JztcclxuXHR4aHIub3BlbihvcHRpb25zLnR5cGUsIG9wdGlvbnMudXJsKTtcclxuXHQvLyBZaWkg0L/RgNC+0LLQtdGA0Y/QtdGCINC90LDQu9C40YfQuNC1INGF0LXQtNC10YDQsCBYLVJlcXVlc3RlZC1XaXRoINCyINGE0LjQu9GM0YLRgNC1IGFqYXhPbmx5XHJcblx0Ly8gaHR0cDovL3d3dy55aWlmcmFtZXdvcmsuY29tL2RvYy9hcGkvMS4xL0NIdHRwUmVxdWVzdCNnZXRJc0FqYXhSZXF1ZXN0LWRldGFpbFxyXG5cdHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdYLVJlcXVlc3RlZC1XaXRoJywnWE1MSHR0cFJlcXVlc3QnKTtcclxuXHR4aHIuc2V0UmVxdWVzdEhlYWRlcignQ29udGVudC1UeXBlJywnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04Jyk7XHJcblx0eGhyLm9ubG9hZCA9IG9wdGlvbnMuc3VjY2VzcztcclxuXHR4aHIuc2VuZChvcHRpb25zLnF1ZXJ5KTtcclxufTtcclxuXHJcbi8vICDilojilogg4paI4paI4paIICAgIOKWiOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIICDilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCDilojilojilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCAg4paI4paIICDilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgICDilojilojilojilojilojilohcclxuLy9cclxuLy8gI2ltcG9ydCAjc3ZnXHJcbmV4cG9ydCBmdW5jdGlvbiBpbXBvcnRTdmcodXJsLCBsb2FkRXZlbnROYW1lLCB0YXJnZXQsIGlkQXR0cmlidXRlKSB7XHJcblx0aW1wb3J0U1ZHKHVybCwgbG9hZEV2ZW50TmFtZSwgdGFyZ2V0LCBpZEF0dHJpYnV0ZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBpbXBvcnRTVkcodXJsLCBsb2FkRXZlbnROYW1lLCB0YXJnZXQsIGlkQXR0cmlidXRlKSB7XHJcblx0bG9hZEV2ZW50TmFtZSA9IGxvYWRFdmVudE5hbWUgfHwgJ3N2Z2xvYWQnO1xyXG5cdHRhcmdldCA9IHRhcmdldCB8fCBkb2N1bWVudC5ib2R5O1xyXG5cdGlmICh0eXBlb2YgRE9NUGFyc2VyICE9PSAndW5kZWZpbmVkJykgLy8gSUU2LVxyXG5cdHJlcXVlc3Qoe1xyXG5cdFx0dXJsOiB1cmwsXHJcblx0XHRzdWNjZXNzOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0Ly8g0JTQu9GPIElFNy05LCDRgi7Qui4g0L7QvdC4INC90LUg0L/QvtC00LTQtdGA0LbQuNCy0LDRjtGCINCy0YHRgtGA0L7QtdC90L3Ri9C5INC/0LDRgNGB0LjQvdCzINGB0LLQvtC50YHRgtCy0LAgcmVzcG9uc2VYTUxcclxuXHRcdFx0Ly8gaHR0cDovL21zZG4ubWljcm9zb2Z0LmNvbS9lbi11cy9saWJyYXJ5L2llL21zNTM1ODc0JTI4dj12cy44NSUyOS5hc3B4XHJcblx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdHZhciB4bWwgPSBwYXJzZXIucGFyc2VGcm9tU3RyaW5nKHRoaXMucmVzcG9uc2VUZXh0LCAndGV4dC94bWwnKTtcclxuXHRcdFx0Ly8gdmFyIHhtbCA9IHRoaXMucmVzcG9uc2VYTUw7XHJcblx0XHRcdHZhciBzdmcgPSBkb2N1bWVudC5pbXBvcnROb2RlKHhtbC5kb2N1bWVudEVsZW1lbnQsIHRydWUpO1xyXG5cdFx0XHRpZiAoaWRBdHRyaWJ1dGUpIHtcclxuXHRcdFx0XHRzdmcuc2V0QXR0cmlidXRlKCdpZCcsIGlkQXR0cmlidXRlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0YXJnZXQuYXBwZW5kQ2hpbGQoc3ZnKTtcclxuXHRcdFx0dGFyZ2V0LmRpc3BhdGNoRXZlbnQobmV3IEN1c3RvbUV2ZW50KGxvYWRFdmVudE5hbWUsIHsgYnViYmxlczogdHJ1ZSB9KSk7XHJcblx0XHR9LFxyXG5cdH0pO1xyXG59O1xyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAgIOKWiOKWiCDilojiloggICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vICDilojilojilojilojiloggICAgIOKWiOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilojilojilojiloggICDilojilogg4paI4paIICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICAgICAg4paI4paIIOKWiOKWiCAgICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNleHRlbmRcclxuZXhwb3J0IGZ1bmN0aW9uIGV4dGVuZChkZXN0LCBzb3VyY2UpIHtcclxuXHRmb3IgKHZhciBwcm9wIGluIHNvdXJjZSkge1xyXG5cdFx0ZGVzdFtwcm9wXSA9IHNvdXJjZVtwcm9wXTtcclxuXHR9XHJcblx0cmV0dXJuIGRlc3Q7XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojiloggICAgICDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNjaGlsZCAjY2xhc3NcclxuZXhwb3J0IGZ1bmN0aW9uIGNoaWxkQnlDbGFzcyhwYXJlbnQsIGNsYXNzTmFtZSkge1xyXG5cdHZhciBjaGlsZHJlbiA9IHBhcmVudC5jaGlsZHJlbjtcclxuXHRmb3IgKHZhciBpID0gMDsgaSA8IGNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRpZiAoY2hpbGRyZW5baV0uY2xhc3NMaXN0LmNvbnRhaW5zKGNsYXNzTmFtZSkpIHtcclxuXHRcdFx0cmV0dXJuIGNoaWxkcmVuW2ldO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNwYWdlICNzY3JvbGxcclxuLy8g0JrRgNC+0YHRgS3QsdGA0LDRg9C30LXRgNC90L7QtSDQvtC/0YDQtdC00LXQu9C10L3QuNC1INCy0LXQu9C40YfQuNC90Ysg0YHQutGA0L7Qu9C70LBcclxuZXhwb3J0IGZ1bmN0aW9uIHBhZ2VTY3JvbGwoKSB7XHJcblx0aWYgKHdpbmRvdy5wYWdlWE9mZnNldCAhPSB1bmRlZmluZWQpIHtcclxuXHRcdHJldHVybiB7XHJcblx0XHRcdGxlZnQ6IHBhZ2VYT2Zmc2V0LFxyXG5cdFx0XHR0b3A6IHBhZ2VZT2Zmc2V0XHJcblx0XHR9O1xyXG5cdH0gZWxzZSB7XHJcblx0XHR2YXIgaHRtbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuXHRcdHZhciBib2R5ID0gZG9jdW1lbnQuYm9keTtcclxuXHJcblx0XHR2YXIgdG9wID0gaHRtbC5zY3JvbGxUb3AgfHwgYm9keSAmJiBib2R5LnNjcm9sbFRvcCB8fCAwO1xyXG5cdFx0dG9wIC09IGh0bWwuY2xpZW50VG9wO1xyXG5cclxuXHRcdHZhciBsZWZ0ID0gaHRtbC5zY3JvbGxMZWZ0IHx8IGJvZHkgJiYgYm9keS5zY3JvbGxMZWZ0IHx8IDA7XHJcblx0XHRsZWZ0IC09IGh0bWwuY2xpZW50TGVmdDtcclxuXHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRsZWZ0OiBsZWZ0LFxyXG5cdFx0XHR0b3A6IHRvcFxyXG5cdFx0fTtcclxuXHR9XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilogg4paI4paIICAgICAgICAgICDilojiloggICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilojilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojiloggICAgICDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNjbG9zZXN0ICNwYXJlbnQgI2NsYXNzXHJcbmV4cG9ydCBmdW5jdGlvbiBjbG9zZXN0UGFyZW50QnlDbGFzcyhlbGVtZW50LCBjbGFzc05hbWUpIHtcclxuXHR2YXIgcGFyZW50ID0gZWxlbWVudC5wYXJlbnROb2RlO1xyXG5cdHdoaWxlICghcGFyZW50LmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpKSB7XHJcblx0XHRwYXJlbnQgPSBwYXJlbnQucGFyZW50Tm9kZTtcclxuXHR9XHJcblx0cmV0dXJuIHBhcmVudDtcclxufVxyXG5cclxuLy8gIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paIICAgICAgIOKWiOKWiCDilojilojilojilojiloggICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vXHJcbi8vICNpcyAjb2JqZWN0XHJcbi8vINCh0L/QtdGA0YLQviDQuNC3IGludGVyYWN0LmpzICjRgdC/0LXRgNGC0L4g0L3QtdC80L3QvtCz0L4sINC90L4g0LLRgdC1INGA0LDQstC90L46IGh0dHA6Ly9pbnRlcmFjdGpzLmlvLylcclxuZXhwb3J0IGZ1bmN0aW9uIGlzT2JqZWN0KHRoaW5nKSB7XHJcblx0cmV0dXJuICEhdGhpbmcgJiYgKHR5cGVvZiB0aGluZyA9PT0gJ29iamVjdCcpO1xyXG59XHJcblxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paIICAgIOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilojilojilogg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy9cclxuLy8gI3JlbVxyXG4vLyDQn9C+0LTRgdGH0ZHRgiDQstC10LvQuNGH0LjQvdGLINCyIHJlbS4g0J3QsCDQstGF0L7QtNC1INC/0LjQutGB0LXQu9C4INCx0LXQtyBgcHhgLCDQvdCwINCy0YvRhdC+0LTQtSDQstC10LvQuNGH0LjQvdCwINCyIHJlbSDRgSBgcmVtYFxyXG52YXIgcm9vdEZvbnRTaXplO1xyXG5leHBvcnQgZnVuY3Rpb24gcmVtKHB4Tm9Vbml0cywgcmVjYWxjKSB7XHJcblx0aWYgKHR5cGVvZiByb290Rm9udFNpemUgIT0gJ251bWJlcicgfHwgcmVjYWxjKSB7XHJcblx0XHQvLyAjQ0JGSVg6IEVkZ2UgKDQyLjE3MTM0LjEuMCwgRWRnZUhUTUwgMTcuMTcxMzQpINC4IElFINC+0L/RgNC10LTQtdC70Y/RjtGCINC30L3QsNGH0LXQvdC40LUg0LrQsNC6LCDQvdCw0L/RgNC40LzQtdGALCA5LjkzINCyINGA0LXQt9GD0LvRjNGC0LDRgtC1ID4+INC+0L/QtdGA0LDRhtC40Y8g0LTQsNC10YIgOSwg0L/QvtGN0YLQvtC80YMg0YLRg9GCINC00L7Qv9C+0LvQvdC40YLQtdC70YzQvdC+INC90LDQtNC+INC+0LrRgNGD0LPQu9GP0YLRjC4gVVBEOiDQl9Cw0YfQtdC8INC/0LXRgNC10LLQvtC00LjRgtGMINCyIGludGVnZXIg0YEg0L/QvtC80L7RidGM0Y4gPj4sINC10YHQu9C4INC80Ysg0YPQttC1INC/0YDQuNC80LXQvdGP0Lwg0Log0YHRgtGA0L7QutC1IE1hdGgucm91bmQsINGC0LXQvCDRgdCw0LzRi9C8INC+0L3QsCDQsNCy0YLQvtC80LDRgtC40YfQtdGB0LrQuCDQutCw0YHRgtGD0LXRgtGB0Y8g0LIgaW50ZWdlci5cclxuXHRcdC8vIHJvb3RGb250U2l6ZSA9IE1hdGgucm91bmQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KS5nZXRQcm9wZXJ0eVZhbHVlKCdmb250LXNpemUnKS5zbGljZSgwLC0yKSkgPj4gMDtcclxuXHRcdHJvb3RGb250U2l6ZSA9IE1hdGgucm91bmQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KS5nZXRQcm9wZXJ0eVZhbHVlKCdmb250LXNpemUnKS5zbGljZSgwLC0yKSk7XHJcblx0fVxyXG5cdHJldHVybiBweE5vVW5pdHMvcm9vdEZvbnRTaXplICsgJ3JlbSc7XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilojilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojiloggICDilojilogg4paI4paIICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojiloggICDilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilogg4paI4paIICAg4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHJcbi8vICNjb250ZW50ICNib3ggI2hlaWdodFxyXG4vLyDQn9C+0LTRgdGH0ZHRgiDQstGL0YHQvtGC0YsgY29udGVudC1ib3gg0YEg0LTRgNC+0LHRjNGOICjQsiDQvtGC0LvQuNGH0LjQtSDQvtGCICQoLi4uKS5oZWlnaHQoKSlcclxuZXhwb3J0IGZ1bmN0aW9uIGNvbnRlbnRCb3hIZWlnaHQoZWxlbWVudCkge1xyXG5cdHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpO1xyXG5cdHJldHVybiBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodFxyXG5cdFx0XHQtIHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ3BhZGRpbmctdG9wJykuc2xpY2UoMCwtMilcclxuXHRcdFx0LSBzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdwYWRkaW5nLWJvdHRvbScpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLXRvcC13aWR0aCcpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLWJvdHRvbS13aWR0aCcpLnNsaWNlKDAsLTIpO1xyXG59XHJcblxyXG4vLyAg4paI4paI4paIICAgIOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCDilojilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojiloggIOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paIICAg4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilojilogg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIICAg4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1xyXG4vLyAjbWFyZ2luICNib3ggI2hlaWdodFxyXG5leHBvcnQgZnVuY3Rpb24gbWFyZ2luQm94SGVpZ2h0KGVsZW1lbnQsIHNvdXJjZVdpbmRvdykge1xyXG5cdHNvdXJjZVdpbmRvdyA9IHNvdXJjZVdpbmRvdyB8fCB3aW5kb3c7XHJcblx0dmFyIHN0eWxlO1xyXG5cdGlmIChlbGVtZW50ICYmIChzdHlsZSA9IHNvdXJjZVdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpKSkge1xyXG5cdFx0cmV0dXJuIGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0XHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLXRvcCcpLnNsaWNlKDAsLTIpID4+IDApXHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLWJvdHRvbScpLnNsaWNlKDAsLTIpID4+IDApO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm4gMDtcclxuXHR9XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojiloggICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICDiloggIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojiloggICAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paIIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy9cclxuLy8gI2NyZWF0ZSAjd2lkZ2V0c1xyXG5cclxuLyoqXHJcbiAqINCh0L7Qt9C00LDQtdGCINC+0LHRitC10LrRgtGLINCy0LjQtNC20LXRgtCwINCyINC+0L/RgNC10LTQtdC70LXQvdC90L7QvCDQutC+0L3RgtC10LrRgdGC0LUuXHJcbiAqIFxyXG4gKiBAcGFyYW0ge2Z1bmN0aW9ufGNsYXNzfSBjb25zdHJ1Y3RvciDQmtC+0L3RgdGC0YDRg9C60YLQvtGAINCy0LjQtNC20LXRgtCwXHJcbiAqIEBwYXJhbSB7RE9NRWxlbWVudH0gY29udGV4dCDQkiDQutC+0L3RgtC10LrRgdGC0LUgKNCy0L3Rg9GC0YDQuCkg0LrQsNC60L7Qs9C+INGN0LvQtdC80LXQvdGC0LAg0L/RgNC+0LjQt9Cy0L7QtNC40YLRjCDQv9C+0LjRgdC6IFxyXG4gKiDRjdC70LXQvNC10L3RgtC+0LIg0LTQu9GPINC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC4INCy0LjQtNC20LXRgtCwLiDQldGB0LvQuCDQvdC1INGD0LrQsNC30LDQvSwg0LjRgdC/0L7Qu9GM0LfRg9C10YLRgdGPIGRvY3VtZW50LlxyXG4gKiBAcGFyYW0ge29iamVjdH0gb3B0aW9ucyDQmtC+0L3RhNC40LPRg9GA0LDRhtC40L7QvdC90YvQuSDQvtCx0YrQtdC60YIsINC/0LXRgNC10LTQsNCy0LDQtdC80YvQuSDRgdC+0LfQtNCw0LLQsNC10LzQvtC80YMg0LLQuNC00LbQtdGC0YNcclxuICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yINCa0LDQutC+0Lkg0YHQtdC70LXQutGC0L7RgCDQuNGB0L/QvtC70YzQt9C+0LLQsNGC0Ywg0LTQu9GPINC/0L7QuNGB0LrQsCDRjdC70LXQvNC10L3RgtC+0LIuIFxyXG4gKiDQldGB0LvQuCDQvdC1INGD0LrQsNC30LDRgtGMLCDQuNGB0L/QvtC70YzQt9GD0LXRgtGB0Y8gbmFtZS5jc3Mg0LjQtyDQv9GA0L7RgtC+0YLQuNC/0LAg0LLQuNC00LbQtdGC0LAuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlV2lkZ2V0cyhjb25zdHJ1Y3RvciwgY29udGV4dCwgb3B0aW9ucywgc2VsZWN0b3IpXHJcbntcclxuXHRpZiAodHlwZW9mIGNvbnN0cnVjdG9yID09ICd1bmRlZmluZWQnKSB7XHJcblx0XHR0aHJvdyBuZXcgRXJyb3IoJ9Cd0YPQttC90L4g0L7QsdGP0LfQsNGC0LXQu9GM0L3QviDRg9C60LDQt9Cw0YLRjCDQutC70LDRgdGBINCy0LjQtNC20LXRgtCwJyk7XHJcblx0fVxyXG5cdGNvbnRleHQgPSBjb250ZXh0IHx8IGRvY3VtZW50O1xyXG5cdHNlbGVjdG9yID0gc2VsZWN0b3IgfHwgJy4nICsgY29uc3RydWN0b3IucHJvdG90eXBlLm5hbWUuY3NzO1xyXG5cclxuXHR2YXIgZWxlbWVudHMgPSBjb250ZXh0LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpO1xyXG5cdGlmIChlbGVtZW50cy5sZW5ndGgpIHtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0Ly8g0J/QvtGH0LXQvNGDLdGC0L4gdHJ5Li4uY2F0Y2gg0LLRi9C00LDQtdGCINC60YPQtNCwINC80LXQvdGM0YjQtSDQv9C+0LvQtdC30L3QvtC5INC40L3RhNC+0YDQvNCw0YbQuNC4LCDRh9C10Lwg0L/RgNC+0YHRgtCw0Y8g0L7QsdGA0LDQsdC+0YLQutCwINC+0YjQuNCx0L7QuiDQsiDQutC+0L3RgdC+0LvQuCDQsdGA0LDRg9C30LXRgNCwXHJcblx0XHRcdC8vIHRyeSB7XHJcblx0XHRcdFx0bmV3IGNvbnN0cnVjdG9yKGVsZW1lbnRzW2ldLCBvcHRpb25zKTtcclxuXHRcdFx0Ly8gfSBjYXRjaChlcnJvcikge1xyXG5cdFx0XHQvLyBcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xyXG5cdFx0XHQvLyB9XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdHRocm93IG5ldyBFcnJvcign0K3Qu9C10LzQtdC90YLRiyDQsiBET00g0LIg0LLRi9Cx0YDQsNC90L3QvtC8INC60L7QvdGC0LXQutGB0YLQtSDQv9C+INGC0LDQutC+0LzRgyDRgdC10LvQtdC60YLQvtGA0YMg0L3QtSDQvdCw0LnQtNC10L3RiycpO1xyXG5cdH1cclxufVxyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilogg4paI4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy8gICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paI4paI4paIICDilojilojilojilojilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojilogg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNzdHJpbmcgI2Nhc2VcclxuZXhwb3J0IGZ1bmN0aW9uIHN0cmluZ0Nhc2UoYml0cywgdHlwZSkge1xyXG5cdGlmICghYml0cyB8fCAhYml0cy5sZW5ndGgpIHtcclxuXHRcdHJldHVybiAnJztcclxuXHR9XHJcblx0aWYgKHR5cGVvZiBiaXRzID09ICdzdHJpbmcnKSB7XHJcblx0XHRiaXRzID0gWyBiaXRzIF07XHJcblx0fVxyXG5cdHZhciBzdHJpbmcgPSAgJyc7XHJcblx0c3dpdGNoICh0eXBlKSB7XHJcblx0XHRjYXNlICdjYW1lbCc6XHJcblx0XHRcdGJpdHMuZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0XHRpZiAoaSA9PSAwKSB7XHJcblx0XHRcdFx0XHRzdHJpbmcgKz0gZmlyc3RMb3dlckNhc2UoYml0KTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0c3RyaW5nICs9IGZpcnN0VXBwZXJDYXNlKGJpdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnY2FwaXRhbENhbWVsJzpcclxuXHRcdFx0Yml0cy5mb3JFYWNoKGZ1bmN0aW9uKGJpdCkge1xyXG5cdFx0XHRcdHN0cmluZyArPSBmaXJzdFVwcGVyQ2FzZShiaXQpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnZmxhdCc6XHJcblx0XHRcdHN0cmluZyA9IGJpdHMuam9pbignJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ3NuYWtlJzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCdfJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ2tlYmFiJzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCctJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ2RvdCc6XHJcblx0XHRcdHN0cmluZyA9IGJpdHMuam9pbignLicpO1xyXG5cdFx0XHRicmVhaztcclxuXHRcclxuXHRcdGNhc2UgJ2FzSXMnOlxyXG5cdFx0ZGVmYXVsdDpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCcnKTtcclxuXHRcdFx0YnJlYWs7XHJcblx0fVxyXG5cdHJldHVybiBzdHJpbmc7XHJcbn1cclxuXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICDilojiloggICAgICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilohcclxuLy8gXHJcbi8vICDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggIOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy9cclxuLy8gI2ZpcnN0ICN1cHBlciAjbG93ZXIgI2Nhc2VcclxuZXhwb3J0IGZ1bmN0aW9uIGZpcnN0VXBwZXJDYXNlKHN0cmluZykge1xyXG5cdHJldHVybiBzdHJpbmdbMF0udG9VcHBlckNhc2UoKSArIHN0cmluZy5zbGljZSgxKTtcclxufTtcclxuZXhwb3J0IGZ1bmN0aW9uIGZpcnN0TG93ZXJDYXNlKHN0cmluZykge1xyXG5cdHJldHVybiBzdHJpbmdbMF0udG9Mb3dlckNhc2UoKSArIHN0cmluZy5zbGljZSgxKTtcclxufTtcclxuXHJcbi8vICNUT0RPOiDQndCw0L/QuNGB0LDRgtGMINGE0YPQvdC60YbQuNC+0L3QsNC7INC70L7Qs9C40L3Qs9CwINC/0LXRgNC10LzQtdC90L3QvtC5INC40LvQuCDQvtCx0YrQtdC60YLQsCDQsiBIVE1MXHJcbi8vIC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojilojilohcclxuLy8gLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIXHJcbi8vIC8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paIIOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAvLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgICDilojilohcclxuLy8gLy8gXHJcbi8vIC8vICDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIXHJcbi8vIC8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy8gLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAvLyAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojilohcclxuLy8gLy9cclxuLy8gLy8gI2RvbSAjbG9nXHJcbi8vIGV4cG9ydCBmdW5jdGlvbiBkb21Mb2cob2JqZWN0LCB0YXJnZXQpIHtcclxuLy8gXHRpZiAodHlwZW9mIHRhcmdldCA9PSAndW5kZWZpbmVkJykge1xyXG4vLyBcdFx0dGFyZ2V0ID0gZG9jdW1lbnQ7XHJcbi8vIFx0fVxyXG4vLyBcdHZhciBvdXRwdXQgPSAnJztcclxuLy8gXHRpZiAodHlwZW9mIG9iamVjdCA9PSAnb2JqZWN0Jykge1xyXG4vLyBcdFx0Zm9yICh2YXIga2V5IGluIG9iamVjdCkge1xyXG4vLyBcdFx0XHRpZiAob2JqZWN0Lmhhc093blByb3BlcnR5KGtleSkpIHtcclxuLy8gXHRcdFx0XHR2YXIgZWxlbWVudCA9IG9iamVjdFtrZXldO1xyXG4vLyBcdFx0XHRcdG91dHB1dCArPSAnJztcclxuLy8gXHRcdFx0fVxyXG4vLyBcdFx0fVxyXG4vLyBcdH1cclxuLy8gXHQnPGRpdiBzdHlsZT1cImZvbnQtZmFtaWx5Om1vbm9zcGFjZTsgZm9udC1zaXplOjFyZW07IHdoaXRlLXNwYWNlOm5vd3JhcDtcIj4nICsgXHJcbi8vIFx0J3dpZHRoICZuYnNwOz0gJyArIHJlY3Qud2lkdGggKyAnPGJyPicgK1xyXG4vLyBcdCdoZWlnaHQgPSAnICsgcmVjdC5oZWlnaHQgKyAnPGJyPicgK1xyXG4vLyBcdCd4ICZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOz0gJyArIHJlY3QueCArICc8YnI+JyArXHJcbi8vIFx0J3kgJm5ic3A7Jm5ic3A7Jm5ic3A7Jm5ic3A7Jm5ic3A7PSAnICsgcmVjdC55ICsgJzxicj4nICtcclxuLy8gXHQnbGVmdCAmbmJzcDsmbmJzcDs9ICcgKyByZWN0LmxlZnQgKyAnPGJyPicgK1xyXG4vLyBcdCd0b3AgJm5ic3A7Jm5ic3A7Jm5ic3A7PSAnICsgcmVjdC50b3AgKyAnPGJyPicgK1xyXG4vLyBcdCdyaWdodCAmbmJzcDs9ICcgKyByZWN0LnJpZ2h0ICsgJzxicj4nICtcclxuLy8gXHQnYm90dG9tID0gJyArIHJlY3QuYm90dG9tICsgJzxicj4nICtcclxuLy8gXHQnPC9kaXY+JztcclxuLy8gfVxyXG5cclxuLy9cdOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vXHTilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICAgICDilojiloggICAgIOKWiOKWiCAg4paI4paIICDilojiloggICAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojiloggICAgICDilojilojilojilojilohcclxuLy9cdCAgICAg4paI4paIICAgIOKWiOKWiCAgICAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggICAg4paI4paIICAgICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vLyAjaW5zZXJ0ICNzdHlsZVxyXG5leHBvcnQgZnVuY3Rpb24gaW5zZXJ0U3R5bGUoKSB7XHJcblx0dmFyIG9wdGlvbnMgPSB7XHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHRjb250ZW50OiBudWxsLFxyXG5cdFx0aW5saW5lOiBudWxsLFxyXG5cdFx0dGFyZ2V0RG9jdW1lbnQ6IG51bGwsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cdFxyXG5cdHZhciBtb3VudEVsZW1lbnQgPSBudWxsO1xyXG5cdFxyXG5cdG9wdGlvbnMuaW5saW5lID0gZmFsc2U7XHJcblx0b3B0aW9ucy50YXJnZXREb2N1bWVudCA9IGRvY3VtZW50O1xyXG5cdFxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaGVhZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICh0eXBlb2YgYXJndW1lbnRzWzBdID09ICdzdHJpbmcnIHx8IGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIFN0cmluZykge1xyXG5cdFx0b3B0aW9ucy51cmwgPSBhcmd1bWVudHNbMF07XHJcblx0XHRvcHRpb25zLm9uTG9hZENhbGxiYWNrID0gYXJndW1lbnRzWzFdO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblx0XHJcblx0aWYgKG9wdGlvbnMudXJsKSB7XHJcblx0XHRpZiAob3B0aW9ucy5pbmxpbmUgPT0gZmFsc2UpIHtcclxuXHRcdFx0dmFyIHRhZyA9IG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGluaycpO1xyXG5cdFx0XHR0YWcucmVsID0gJ3N0eWxlc2hlZXQnO1xyXG5cdFx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRcdHRhZy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgb3B0aW9ucy5vbkxvYWRDYWxsYmFjayk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGFnLmhyZWYgPSBvcHRpb25zLnVybDtcclxuXHRcdFx0b3B0aW9ucy50YXJnZXREb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblx0XHRcdHhoci5vcGVuKCdnZXQnLCBvcHRpb25zLnVybCk7XHJcblx0XHRcdHhoci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgdGFnID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xyXG5cdFx0XHRcdHRhZy50ZXh0Q29udGVudCA9IHRoaXMucmVzcG9uc2VUZXh0O1xyXG5cdFx0XHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRcdHhoci5zZW5kKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmNvbnRlbnQpIHtcclxuXHRcdHZhciB0YWcgPSBvcHRpb25zLnRhcmdldERvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyk7XHJcblx0XHR0YWcudGV4dENvbnRlbnQgPSBvcHRpb25zLmNvbnRlbnQ7XHJcblx0XHRvcHRpb25zLm1vdW50RWxlbWVudC5hcHBlbmRDaGlsZCh0YWcpO1xyXG5cdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjaygpO1xyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG59XHJcblxyXG4vL1x04paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilojilojilojilojiloggIOKWiOKWiCDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x0ICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vIFxyXG4vL1x0I2luc2VydCAjc2NyaXB0XHJcbmV4cG9ydCBmdW5jdGlvbiBpbnNlcnRTY3JpcHQoKSB7XHJcblx0dmFyIG9wdGlvbnMgPSB7XHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHRjb250ZW50OiBudWxsLFxyXG5cdFx0aW5saW5lOiBudWxsLFxyXG5cdFx0dGFyZ2V0RG9jdW1lbnQ6IG51bGwsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cclxuXHR2YXIgbW91bnRFbGVtZW50ID0gbnVsbDtcclxuXHJcblx0b3B0aW9ucy5pbmxpbmUgPSBmYWxzZTtcclxuXHRvcHRpb25zLnRhcmdldERvY3VtZW50ID0gZG9jdW1lbnQ7XHJcblxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaGVhZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cclxuXHRpZiAodHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnc3RyaW5nJyB8fCBhcmd1bWVudHNbMF0gaW5zdGFuY2VvZiBTdHJpbmcpIHtcclxuXHRcdG9wdGlvbnMudXJsID0gYXJndW1lbnRzWzBdO1xyXG5cdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjayA9IGFyZ3VtZW50c1sxXTtcclxuXHR9IGVsc2UgaWYgKCEhYXJndW1lbnRzWzBdICYmIHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ29iamVjdCcpIHtcclxuXHRcdGV4dGVuZChvcHRpb25zLCBhcmd1bWVudHNbMF0pO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG5cclxuXHR2YXIgdGFnID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuXHJcblx0aWYgKG9wdGlvbnMudXJsKSB7XHJcblx0XHRpZiAob3B0aW9ucy5pbmxpbmUgPT0gZmFsc2UpIHtcclxuXHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHR0YWcuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2spO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRhZy5zcmMgPSBvcHRpb25zLnVybDtcclxuXHRcdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQodGFnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHRcdFx0eGhyLm9wZW4oJ2dldCcsIG9wdGlvbnMudXJsKTtcclxuXHRcdFx0eGhyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHRhZy50ZXh0Q29udGVudCA9IHRoaXMucmVzcG9uc2VUZXh0O1xyXG5cdFx0XHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRcdHhoci5zZW5kKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmNvbnRlbnQpIHtcclxuXHRcdHRhZy50ZXh0Q29udGVudCA9IG9wdGlvbnMuY29udGVudDtcclxuXHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRvcHRpb25zLm9uTG9hZENhbGxiYWNrKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcbn1cclxuXHJcbi8vXHTilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojilogg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vL1x04paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paI4paIIOKWiOKWiFxyXG4vL1x04paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojiloggIOKWiOKWiOKWiOKWiCDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCDilojilogg4paI4paIXHJcbi8vXHTilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIXHJcbi8vXHTilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vXHQjaW5zZXJ0XHJcbmV4cG9ydCBmdW5jdGlvbiBpbnNlcnRIdG1sKCkge1xyXG5cdHZhciBvcHRpb25zID0ge1xyXG5cdFx0aHRtbDogbnVsbCxcclxuXHRcdGxlZ2FjeTogZmFsc2UsXHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHR0YXJnZXREb2N1bWVudDogZG9jdW1lbnQsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cdFxyXG5cdHZhciBtb3VudEVsZW1lbnQgPSBudWxsO1xyXG5cdFxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuYm9keTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICh0eXBlb2YgYXJndW1lbnRzWzBdID09ICdzdHJpbmcnIHx8IGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIFN0cmluZykge1xyXG5cdFx0b3B0aW9ucy5odG1sID0gYXJndW1lbnRzWzBdO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblxyXG5cdGlmIChvcHRpb25zLnVybCkge1xyXG5cdFx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cdFx0eGhyLm9wZW4oJ2dldCcsIG9wdGlvbnMudXJsKTtcclxuXHRcdHhoci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG9wdGlvbnMubGVnYWN5KSB7XHJcblx0XHRcdFx0Ly8gI0NCTk9URTog0KHRgtCw0YDRi9C1INCx0YDQsNGD0LfQtdGA0YssINGC0LDQutC40LUg0LrQsNC6IFdlYlZpZXcgMzMsINC90LUg0L/QvtC00LTQtdGA0LbQuNCy0LDRjtGCIGNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudCAo0LjQu9C4INGH0YLQvi3RgtC+INCyINGN0YLQvtC8INC00YPRhdC1KSwg0LIg0L7QsdGJ0LXQvCwg0LXRgdC70Lgg0YDRg9Cz0LDRjtGC0YHRjywg0YLQviDQstC+0YIg0YLQsNC6INC80L7QttC90L4g0L7QsdC+0LnRgtC4OlxyXG5cdFx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdFx0dmFyIGV4dGVybmFsRG9tID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZygnPGRpdiBpZD1cIl9faW1wb3J0ZWRfaHRtbF9fXCI+JyArIHRoaXMucmVzcG9uc2VUZXh0ICsgJzwvZGl2PicsICd0ZXh0L2h0bWwnKTtcclxuXHRcdFx0XHQvLyAjVE9ETzog0JIg0LTQsNC90L3QvtC8INGB0LvRg9GH0LDQtSDQstGB0YLQsNCy0LjRgtGB0Y8gPGRpdiBpZD1cIl9faW1wb3J0ZWRfaHRtbF9fXCI+Li4u0L/QvtC70YPRh9C10L3QvdGL0LkgSFRNTC4uLjwvZGl2Piwg0Y3RgtC+INC90LDQtNC+INC40YHQv9GA0LDQstC40YLRjCDQvdCwINC90LXQutGD0Y4g0LLQvtC30LzQvtC20L3QviDQv9C+0YHQu9C10LTQvtCy0LDRgtC10LvRjNC90YPRjiDQstGB0YLQsNCy0LrRgyDQstGB0LXRhSDQtNC+0YfQtdGA0L3QuNGFINC90L7QtNC+0LIg0Y3RgtC+0LPQviBkaXYnYVxyXG5cdFx0XHRcdHZhciBkb21GcmFnbWVudCA9IG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaW1wb3J0Tm9kZShleHRlcm5hbERvbS5nZXRFbGVtZW50QnlJZCgnX19pbXBvcnRlZF9odG1sX18nKSwgdHJ1ZSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dmFyIGRvbUZyYWdtZW50ID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVSYW5nZSgpLmNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudCh0aGlzLnJlc3BvbnNlVGV4dCk7XHJcblx0XHRcdH1cclxuXHRcdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9tRnJhZ21lbnQpO1xyXG5cdFx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcdHhoci5zZW5kKCk7XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmh0bWwpIHtcclxuXHRcdGlmIChvcHRpb25zLmxlZ2FjeSkge1xyXG5cdFx0XHQvLyAjQ0JOT1RFOiDQodGC0LDRgNGL0LUg0LHRgNCw0YPQt9C10YDRiywg0YLQsNC60LjQtSDQutCw0LogV2ViVmlldyAzMywg0L3QtSDQv9C+0LTQtNC10YDQttC40LLQsNGO0YIgY3JlYXRlQ29udGV4dHVhbEZyYWdtZW50ICjQuNC70Lgg0YfRgtC+LdGC0L4g0LIg0Y3RgtC+0Lwg0LTRg9GF0LUpLCDQsiDQvtCx0YnQtdC8LCDQtdGB0LvQuCDRgNGD0LPQsNGO0YLRgdGPLCDRgtC+INCy0L7RgiDRgtCw0Log0LzQvtC20L3QviDQvtCx0L7QudGC0Lg6XHJcblx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdHZhciBleHRlcm5hbERvbSA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcoJzxkaXYgaWQ9XCJfX2ltcG9ydGVkX2h0bWxfX1wiPicgKyBvcHRpb25zLmh0bWwgKyAnPC9kaXY+JywgJ3RleHQvaHRtbCcpO1xyXG5cdFx0XHR2YXIgZG9tRnJhZ21lbnQgPSBvcHRpb25zLnRhcmdldERvY3VtZW50LmltcG9ydE5vZGUoZXh0ZXJuYWxEb20uZ2V0RWxlbWVudEJ5SWQoJ19faW1wb3J0ZWRfaHRtbF9fJyksIHRydWUpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dmFyIGRvbUZyYWdtZW50ID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVSYW5nZSgpLmNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudChvcHRpb25zLmh0bWwpO1xyXG5cdFx0fVxyXG5cdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9tRnJhZ21lbnQpO1xyXG5cdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjaygpO1xyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG59XHJcblxyXG4vL1x04paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIICDilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy9cdCAgICAg4paI4paIICDilojiloggIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiCAgICDilojilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilohcclxuLy9cdOKWiOKWiCAgICAgICDilojiloggIOKWiOKWiCAg4paI4paI4paI4paIICDilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggICDilojilojilojiloggICDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHQgICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vL1x0I2luc2VydCAjc3ZnICNzeW1ib2xcclxuZXhwb3J0IGZ1bmN0aW9uIGluc2VydFN2Z1N5bWJvbCgpIHtcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gINCS0LDRgNC40LDQvdGCINGBINC+0YLQtNC10LvRjNC90YvQvCDRgNC+0LTQuNGC0LXQu9GM0YHQutC40LxcclxuXHQvLyBcdFNWRy3RjdC70LXQvNC10L3RgtC+0Lwg0LTQu9GPINC60LDQttC00L7Qs9C+INGB0LjQvNCy0L7Qu9CwOlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHR2YXIgb3B0aW9ucyA9IHtcclxuXHRcdHN5bWJvbDogbnVsbCxcclxuXHRcdG1vdW50RWxlbWVudDogZG9jdW1lbnQuYm9keSxcclxuXHRcdGNvbnRhaW5lckNsYXNzOiBudWxsLFxyXG5cdH07XHJcblx0XHJcblx0aWYgKHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ3N0cmluZycgfHwgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgU3RyaW5nKSB7XHJcblx0XHRvcHRpb25zLnN5bWJvbCA9IGFyZ3VtZW50c1swXTtcclxuXHRcdG9wdGlvbnMubW91bnRFbGVtZW50ID0gYXJndW1lbnRzWzFdIHx8IG9wdGlvbnMubW91bnRFbGVtZW50O1xyXG5cdFx0b3B0aW9ucy5jb250YWluZXJDbGFzcyA9IGFyZ3VtZW50c1syXSB8fCBvcHRpb25zLmNvbnRhaW5lckNsYXNzO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblxyXG5cdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0dmFyIHN5bWJvbERvY3VtZW50ID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyhcclxuXHRcdGA8c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiBcclxuXHRcdFx0JHsgb3B0aW9ucy5jb250YWluZXJDbGFzcyA/IGBjbGFzcz1cIiR7b3B0aW9ucy5jb250YWluZXJDbGFzc31cImAgOiAnJyB9IFxyXG5cdFx0XHRzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XHJcblx0XHRcdCR7b3B0aW9ucy5zeW1ib2x9XHJcblx0XHQ8L3N2Zz5gLFxyXG5cdFx0J2ltYWdlL3N2Zyt4bWwnXHJcblx0KTtcclxuXHR2YXIgc3ltYm9sTm9kZSA9IGRvY3VtZW50LmltcG9ydE5vZGUoc3ltYm9sRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0cnVlKTtcclxuXHRvcHRpb25zLm1vdW50RWxlbWVudC5hcHBlbmRDaGlsZChzeW1ib2xOb2RlKTtcclxuXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vICDQktCw0YDQuNCw0L3RgiDRgdC+INCy0YHRgtCw0LLQutC+0Lkg0LIg0L7QtNC40L0g0YDQvtC00LjRgtC10LvRjNGB0LrQuNC5IFxyXG5cdC8vIFx0U1ZHLdGN0LvQtdC80LXQvdGCOlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyB2YXIgb3B0aW9ucyA9IHtcclxuXHQvLyBcdHN5bWJvbDogbnVsbCxcclxuXHQvLyBcdGNvbnRhaW5lcklkOiAnc3ZnX3Nwcml0ZScsXHJcblx0Ly8gXHRtb3VudEVsZW1lbnQ6IGRvY3VtZW50LmJvZHksXHJcblx0Ly8gfTtcclxuXHQgXHJcblx0Ly8gaWYgKHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ3N0cmluZycgfHwgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgU3RyaW5nKSB7XHJcblx0Ly8gXHRvcHRpb25zLnN5bWJvbCA9IGFyZ3VtZW50c1swXTtcclxuXHQvLyBcdG9wdGlvbnMuY29udGFpbmVySWQgPSBhcmd1bWVudHNbMV0gfHwgb3B0aW9ucy5jb250YWluZXJJZDtcclxuXHQvLyBcdG9wdGlvbnMubW91bnRFbGVtZW50ID0gYXJndW1lbnRzWzJdIHx8IG9wdGlvbnMubW91bnRFbGVtZW50O1xyXG5cdC8vIH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdC8vIFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0Ly8gfSBlbHNlIHtcclxuXHQvLyBcdHJldHVybjtcclxuXHQvLyB9XHJcblxyXG5cdC8vIHZhciBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChvcHRpb25zLmNvbnRhaW5lcklkKTtcclxuXHRcclxuXHQvLyBpZiAoY29udGFpbmVyID09IG51bGwpIHtcclxuXHQvLyBcdGNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUygnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLCAnc3ZnJyk7XHJcblx0Ly8gXHRjb250YWluZXIuc2V0QXR0cmlidXRlKCdpZCcsIG9wdGlvbnMuY29udGFpbmVySWQpO1xyXG5cdC8vIFx0Y29udGFpbmVyLnN0eWxlLmNzc1RleHQgPSAnZGlzcGxheTogbm9uZTsnO1xyXG5cdC8vIFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoY29udGFpbmVyKTtcclxuXHQvLyB9XHJcblxyXG5cdC8vIHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0Ly8gdmFyIHN5bWJvbERvY3VtZW50ID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyhcclxuXHQvLyBcdGA8c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiBzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XHJcblx0Ly8gXHRcdCR7b3B0aW9ucy5zeW1ib2x9XHJcblx0Ly8gXHQ8L3N2Zz5gLFxyXG5cdC8vIFx0J2ltYWdlL3N2Zyt4bWwnXHJcblx0Ly8gKTtcclxuXHQvLyB2YXIgc3ltYm9sTm9kZSA9IGRvY3VtZW50LmltcG9ydE5vZGUoc3ltYm9sRG9jdW1lbnQucXVlcnlTZWxlY3Rvcignc3ltYm9sJyksIHRydWUpO1xyXG5cdC8vIGNvbnRhaW5lci5hcHBlbmRDaGlsZChzeW1ib2xOb2RlKTtcclxufSIsImltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4vdXRpbHMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gU3RyaW5nQ2FzZShiaXRzKSB7XHJcblx0dGhpcy5iaXRzID0gYml0cztcclxuXHJcblx0aWYgKEFycmF5LmlzQXJyYXkoYml0cykpIHtcclxuXHRcdHRoaXMuYXNJcyA9IHRoaXMuYml0cy5qb2luKCcnKTtcclxuXHRcdGJpdHMuZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0Yml0c1tpXSA9IGJpdC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0fSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHRoaXMuYXNJcyA9IHRoaXMuYml0cztcclxuXHRcdGJpdHMgPSBiaXRzLnRvTG93ZXJDYXNlKCk7XHJcblx0fVxyXG5cclxuXHR0aGlzLmNhbWVsID0gdXRpbHMuc3RyaW5nQ2FzZShiaXRzLCAnY2FtZWwnKTtcclxuXHR0aGlzLmNhcGl0YWxDYW1lbCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2NhcGl0YWxDYW1lbCcpO1xyXG5cdHRoaXMua2ViYWIgPSB1dGlscy5zdHJpbmdDYXNlKGJpdHMsICdrZWJhYicpO1xyXG5cdHRoaXMuc25ha2UgPSB1dGlscy5zdHJpbmdDYXNlKGJpdHMsICdzbmFrZScpO1xyXG5cdHRoaXMuZmxhdCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2ZsYXQnKTtcclxuXHR0aGlzLmRvdCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2RvdCcpO1xyXG59XHJcblN0cmluZ0Nhc2UucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XHJcblx0cmV0dXJuIHRoaXMuYXNJcztcclxufTsiLCJpbXBvcnQgKiBhcyB1dGlscyBmcm9tICcuL3V0aWxzJztcclxuaW1wb3J0IFN0cmluZ0Nhc2UgZnJvbSAnLi9TdHJpbmdDYXNlJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE5hbWUobmFtZUJpdHMsIG5zQml0cykge1xyXG5cdHZhciBiaXRzO1xyXG5cclxuXHQvLyDQlNCw0LvQtdC1INC40LTQtdGCINC+0LHRgNCw0LHQvtGC0LrQsCDQv9Cw0YDQsNC80LXRgtGA0LAgbnNCaXRzOlxyXG5cdC8vIC0g0LXRgdC70Lgg0L7QvSDQvdC1INC30LDQtNCw0L0sINC/0YDQvtC/0YPRgdGC0LjRgtGMINCy0YHQtSDQuCDQstC30Y/RgtGMIG5zINC40Lcg0L/RgNC+0YLQvtGC0LjQv9CwXHJcblx0Ly8gLSDQtdGB0LvQuCDQvtC9IGJvb2xlYW4g0LggdHJ1ZSwg0YLQviDRgtCw0LrQttC1INC/0YDQvtC/0YPRgdGC0LjRgtGMINC4INC40YHQv9C+0LvRjNC30L7QstCw0YLRjCBucyBcclxuXHQvLyAgINC40Lcg0L/RgNC+0YLQvtGC0LjQv9CwXHJcblx0Ly8gLSDQtdGB0LvQuCDQvtC9IGJvb2xlYW4g0LggZmFsc2UsINGC0L4gbnNCaXRzIC0g0L/Rg9GB0YLQsNGPINGB0YLRgNC+0LrQsFxyXG5cdC8vIC0g0Lgg0LXRgdC70Lgg0LIg0YDQtdC30YPQu9GM0YLQsNGC0LUgbnNCaXRzINGB0YLRgNC+0LrQsCDQuNC70Lgg0LzQsNGB0YHQuNCyLCDRgtC+INGB0L7Qt9C00LDRgtGMINC90L7QstC+0LUgXHJcblx0Ly8gICDRgdCy0L7QudGB0YLQstC+IG5zINC/0L7QstC10YDRhSDQs9C10YLRgtC10YDQsCDQv9GA0L7RgtC+0YLQuNC/0LAg0YHQviDQt9C90LDRh9C10L3QuNC10LwgbnNCaXRzXHJcblx0aWYgKG5zQml0cyAhPSBudWxsKSB7XHJcblx0XHRpZiAodHlwZW9mIG5zQml0cyA9PSAnYm9vbGVhbicgJiYgbnNCaXRzID09IGZhbHNlKSB7XHJcblx0XHRcdG5zQml0cyA9ICcnO1xyXG5cdFx0fVxyXG5cdFx0aWYgKHR5cGVvZiBuc0JpdHMgPT0gJ3N0cmluZycgfHwgQXJyYXkuaXNBcnJheShuc0JpdHMpKSB7XHJcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnbnMnLCB7XHJcblx0XHRcdFx0dmFsdWU6IG5ldyBTdHJpbmdDYXNlKG5zQml0cyksXHJcblx0XHRcdFx0d3JpdGFibGU6IHRydWUsXHJcblx0XHRcdFx0Y29uZmlndXJhYmxlOiB0cnVlLFxyXG5cdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0dmFyIGhhc05zID0gdGhpcy5ucyAmJiB0aGlzLm5zLnRvU3RyaW5nKCkgIT0gJyc7XHJcblxyXG5cdHRoaXMuYmFzZSA9IG5ldyBTdHJpbmdDYXNlKG5hbWVCaXRzKTtcclxuXHJcblx0Ly8gI2FzSXMgI2ZsYXQgI2NhbWVsICNjYXBpdGFsICNrZWJhYlxyXG5cdHZhciB0eXBlcyA9IFsnYXNJcycsICdmbGF0JywgJ2NhbWVsJywgJ2NhcGl0YWxDYW1lbCcsICdrZWJhYiddO1xyXG5cdGZvciAodmFyIGkgPSAwOyBpIDwgdHlwZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdGlmIChoYXNOcykge1xyXG5cdFx0XHR0aGlzWydfJyArIHR5cGVzW2ldXSA9IHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMubnNbdHlwZXNbaV1dLCB0aGlzLmJhc2VbdHlwZXNbaV1dXSwgdHlwZXNbaV0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dGhpc1snXycgKyB0eXBlc1tpXV0gPSB0aGlzLmJhc2VbdHlwZXNbaV1dO1xyXG5cdFx0fVxyXG5cdFx0dGhpc1t0eXBlc1tpXV0gPSAoZnVuY3Rpb24odHlwZSwgbmFtZU9iaikge1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oc3VmZml4KSB7XHJcblx0XHRcdFx0aWYgKHN1ZmZpeCkge1xyXG5cdFx0XHRcdFx0aWYgKHR5cGUgIT0gJ2FzSXMnKSB7XHJcblx0XHRcdFx0XHRcdGlmIChBcnJheS5pc0FycmF5KHN1ZmZpeCkpIHtcclxuXHRcdFx0XHRcdFx0XHRzdWZmaXguZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHN1ZmZpeFtpXSA9IGJpdC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm4gdXRpbHMuc3RyaW5nQ2FzZShbbmFtZU9ialsnXycgKyB0eXBlXSwgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsIHR5cGUpXSwgdHlwZSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHJldHVybiBuYW1lT2JqWydfJyArIHR5cGVdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdH0pKHR5cGVzW2ldLCB0aGlzKTtcclxuXHR9XHJcblxyXG5cdC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAgICDilojilohcclxuXHQvLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojiloggICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICAgIOKWiOKWiFxyXG5cdC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG5cdC8vXHJcblx0Ly8gI2RvdFxyXG5cdHZhciBiaXRzID0gWyB0aGlzLmJhc2UuY2FtZWwgXTtcclxuXHRpZiAoaGFzTnMpIHtcclxuXHRcdGJpdHMudW5zaGlmdCh0aGlzLm5zLmNhbWVsKTtcclxuXHR9XHJcblx0dGhpcy5fZG90ID0gdXRpbHMuc3RyaW5nQ2FzZShiaXRzLCAnZG90Jyk7XHJcblx0dGhpcy5kb3QgPSBmdW5jdGlvbihzdWZmaXgpIHtcclxuXHRcdGlmIChzdWZmaXgpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkoc3VmZml4KSkge1xyXG5cdFx0XHRcdHN1ZmZpeC5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0c3VmZml4W2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMuX2RvdCwgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsICdjYW1lbCcpXSwgJ2RvdCcpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX2RvdDtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQvLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG5cdC8vICDilojilojilojilojiloggICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojilohcclxuXHQvLyAg4paI4paIICAgICAgIOKWiOKWiCAg4paI4paIICDilojiloggICAgICDilojiloggIOKWiOKWiCDilojiloggICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojiloggICAg4paI4paIXHJcblx0Ly9cclxuXHQvLyAjZXZlbnRcclxuXHR0aGlzLmV2ZW50ID0gdGhpcy5kb3Q7XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly9cclxuXHQvLyAjY3NzXHJcblx0Yml0cyA9IFsgdGhpcy5iYXNlLnNuYWtlIF07XHJcblx0aWYgKGhhc05zKSB7XHJcblx0XHRiaXRzLnVuc2hpZnQodGhpcy5ucy5zbmFrZSk7XHJcblx0fVxyXG5cdHRoaXMuX2NzcyA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2tlYmFiJyk7XHJcblx0dGhpcy5jc3MgPSBmdW5jdGlvbihzdWZmaXgpIHtcclxuXHRcdGlmIChzdWZmaXgpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkoc3VmZml4KSkge1xyXG5cdFx0XHRcdHN1ZmZpeC5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0c3VmZml4W2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMuX2NzcywgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsICdzbmFrZScpXSwgJ2tlYmFiJyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fY3NzO1xyXG5cdFx0fVxyXG5cdH07XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gXHJcblx0Ly8gIOKWiOKWiOKWiCAgICDilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCDilojilojilojilogg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vXHJcblx0Ly8gI2NzcyAjbW9kaWZpY2F0b3JcclxuXHR0aGlzLmNzc01vZGlmaWNhdG9yID0gZnVuY3Rpb24obW9kaWZpY2F0b3JOYW1lKSB7XHJcblx0XHRpZiAobW9kaWZpY2F0b3JOYW1lICE9IG51bGwpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkobW9kaWZpY2F0b3JOYW1lKSkge1xyXG5cdFx0XHRcdG1vZGlmaWNhdG9yTmFtZS5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0bW9kaWZpY2F0b3JOYW1lW2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXMuY3NzKCkgKyAnLS0nICsgdXRpbHMuc3RyaW5nQ2FzZShtb2RpZmljYXRvck5hbWUsICdzbmFrZScpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuY3NzKCk7XHJcblx0XHR9XHJcblx0fTtcclxuXHR0aGlzLmNzc01vZCA9IHRoaXMuY3NzTW9kaWZpY2F0b3I7XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gXHJcblx0Ly8gICDilojilojilojilojilojilogg4paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiFxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vIFxyXG5cdC8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojiloggICAgICDilojilojilojilojiloggICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilohcclxuXHQvLyAgICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuXHQvL1xyXG5cdC8vICNjc3MgI2NsYXNzICNzZWxlY3RvclxyXG5cdHRoaXMuY3NzQ2xhc3NTZWxlY3RvciA9IGZ1bmN0aW9uKGFwcGVuZGFnZSkge1xyXG5cdFx0cmV0dXJuICcuJyArIHRoaXMuY3NzKGFwcGVuZGFnZSk7XHJcblx0fTtcclxuXHR0aGlzLnNlbGVjdG9yID0gZnVuY3Rpb24oYXBwZW5kYWdlKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5jc3NDbGFzc1NlbGVjdG9yKGFwcGVuZGFnZSk7XHJcblx0fTtcclxufVxyXG52YXIgbnM7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShOYW1lLnByb3RvdHlwZSwgJ25zJywge1xyXG5cdHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdG5zID0gbmV3IFN0cmluZ0Nhc2UodmFsdWUpO1xyXG5cdH0sXHJcblx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdHJldHVybiBucztcclxuXHR9LFxyXG59KTtcclxuTmFtZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcy5fYXNJcztcclxufTsiLCJ2YXIgY3Jvc3NTdHJva2UgPSBcIjxzeW1ib2wgaWQ9XFxcImVnbWwtaWNvbnMtY3Jvc3Mtc3Ryb2tlXFxcIiB2aWV3Qm94PVxcXCIwIDAgMjAgMjBcXFwiPjxwYXRoIGQ9XFxcIk0gMTYuNTg5OTIyLDIuMDAwMDE0MyAxMCw4LjU4OTAwNDkgMy40MTIwNTQ3LDIuMDAxOTkwOCAxLjk5OTk0NTMsMy40MTIwMDkyIDguNTg5ODQzOCwxMC4wMDA5NzYgMS45OTk5MjE5LDE2LjU4Nzk5MSAzLjQxMDA3ODEsMTcuOTk4MDA5IDEwLDExLjQxMDk5NSBsIDYuNTg5OTIyLDYuNTg5MDE0IDEuNDEwMTU2LC0xLjQxMDAxOCAtNi41ODk5MjIsLTYuNTg5MDE1IDYuNTg5OTIyLC02LjU4ODk5MDMgelxcXCIgY2xpcC1ydWxlPVxcXCJldmVub2RkXFxcIiBmaWxsLXJ1bGU9XFxcImV2ZW5vZGRcXFwiLz48L3N5bWJvbD5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyb3NzU3Ryb2tlO1xyXG4iLCJ2YXIgY29nU3Ryb2tlID0gXCI8c3ltYm9sIGlkPVxcXCJlZ21sLWljb25zLWNvZy1zdHJva2VcXFwiIHZpZXdCb3g9XFxcIjAgMCAxMDAgMTAwXFxcIj48cGF0aCBzdHlsZT1cXFwibGluZS1oZWlnaHQ6bm9ybWFsO3RleHQtaW5kZW50OjA7dGV4dC1hbGlnbjpzdGFydDt0ZXh0LWRlY29yYXRpb24tbGluZTpub25lO3RleHQtZGVjb3JhdGlvbi1zdHlsZTpzb2xpZDt0ZXh0LWRlY29yYXRpb24tY29sb3I6IzAwMDt0ZXh0LXRyYW5zZm9ybTpub25lO2Jsb2NrLXByb2dyZXNzaW9uOnRiO3doaXRlLXNwYWNlOm5vcm1hbDtpc29sYXRpb246YXV0bzttaXgtYmxlbmQtbW9kZTpub3JtYWw7c29saWQtY29sb3I6IzAwMDtzb2xpZC1vcGFjaXR5OjFcXFwiIGQ9XFxcIk0gMzcuMjczNDM4LDAgMzYuNDQxNDA2LDMuNTY2NDA2MiAzMy41NjQ0NTMsMTUuODkyNTc4IGMgLTEuNTQwOTgsMC43NDk2MjcgLTMuMDE5NjU4LDEuNTk2MTY2IC00LjQzOTQ1MywyLjU0Mjk2OSBMIDEzLjU1NDY4OCwxMy40NDMzNTkgMC4zODY3MTg3NSwzNi43NDYwOTQgMTIuMjgxMjUsNDcuMTc1NzgxIGMgLTAuMDgxODgsMC45MzgxMTYgLTAuMjIwNzAzLDEuODMzMTc4IC0wLjIyMDcwMywyLjgyMjI2NiAwLDAuODUyNjM1IDAuMTEzNzUsMS42MTc2OTMgMC4xNzU3ODEsMi40Mjc3MzQgTCAwLjIyMjY1NjI1LDYyLjk1NTA3OCAxMy4zOTA2MjUsODYuMjYxNzE5IDI4Ljc3MTQ4NCw4MS4zMzIwMzEgYyAxLjQ2NDA4LDAuOTk4MDQ0IDIuOTkxMDE1LDEuODg1NDM3IDQuNTgyMDMyLDIuNjY5OTIyIGwgMi43ODkwNjIsMTUuMzM1OTM4IDI2Ljc1LDAuNjYyMTA5IDMuNzI0NjEsLTE1Ljk3MDcwMyBjIDEuMzkxODYyLC0wLjY4NzU2MyAyLjczMjYxMSwtMS40NDk2NzQgNC4wMjE0ODQsLTIuMjk2ODc1IEwgODUuMTQ4NDM4LDg3LjE3OTY4OCA5OS40NDE0MDYsNjQuNTQ0OTIyIDg3LjY3NzczNCw1My4xNzE4NzUgYyAwLjEwMjQ2OCwtMS4wNTAzODggMC4yNjE3MTksLTIuMDU5NDYzIDAuMjYxNzE5LC0zLjE3MzgyOCAwLC0wLjc2MzI1NSAtMC4wOTgyNCwtMS40NDY2OTUgLTAuMTQ4NDM3LC0yLjE3MzgyOCBMIDk5Ljc3NzM0NCwzNy4zMjAzMTIgODYuNjAzNTE2LDE0LjAxNzU3OCA3MS41MTE3MTksMTguODU1NDY5IGMgLTEuNDk1ODIzLC0xLjAzOTE3IC0zLjA1NzMwMiwtMS45NjMyNjYgLTQuNjg3NSwtMi43NzczNDQgTCA2NC4wMjM0MzgsMC42NjIxMDkzOCAzNy4yNzM0MzgsMCBaIG0gNy4wOTk2MDksOS4xNzc3MzQ0IDEyLjEwNTQ2OSwwLjMwMDc4MTIgMi4zMzAwNzgsMTIuODIwMzEyNCAyLjI2NTYyNSwwLjkzOTQ1MyBjIDIuNDU3OTEyLDEuMDE4OTY2IDQuNzgwNzM0LDIuNDAxNTI0IDYuOTE0MDYyLDQuMDk3NjU3IGwgMS44ODQ3NjYsMS40OTYwOTMgMTIuNTAxOTUzLC00LjAwNzgxMiA1Ljk2NDg0NCwxMC41NTI3MzQgLTkuOTQxNDA2LDguNzEyODkxIDAuMjkxMDE1LDIuMzY1MjM0IGMgMC4xNTMwOTQsMS4yNDEwNTUgMC4yNSwyLjQxNjUwOCAwLjI1LDMuNTQyOTY5IDAsMS4zNjI5NjkgLTAuMTM5MzY4LDIuNzg1Mjg2IC0wLjM2MTMyOCw0LjI3MzQzNyBsIC0wLjM0MTc5NywyLjI4OTA2MyA5LjY4NzUsOS4zNjcxODcgLTYuNDcwNzAzLDEwLjI1IC0xMi4xMDU0NjksLTQuNTQ0OTIyIC0xLjk3NDYwOSwxLjQ4NjMyOSBjIC0yLjAwMzI2NiwxLjUwNzY0OSAtNC4xMzQ4NDksMi43NDY5NzIgLTYuMzY5MTQxLDMuNjY2MDE1IGwgLTIuMTQ0NTMxLDAuODgwODYgLTMuMDY2NDA2LDEzLjE1NjI1IEwgNDMuNjg3NSw5MC41MjE0ODQgNDEuMzY1MjM0LDc3Ljc0ODA0NyAzOS4wNzgxMjUsNzYuODE0NDUzIGMgLTIuNDI1OTg1LC0wLjk4OTQ3NSAtNC43MjE2NiwtMi4zMzE3NjQgLTYuODM5ODQ0LC0zLjk4MjQyMiBsIC0xLjg3Njk1MywtMS40NjA5MzcgLTEyLjczODI4MSw0LjA4Mzk4NCAtNS45NjQ4NDQsLTEwLjU1NDY4NyA5Ljk5NDE0MSwtOC43NTk3NjYgLTAuMzEyNSwtMi4zODQ3NjYgYyAtMC4xNzIwMywtMS4zMTQxODYgLTAuMjc5Mjk3LC0yLjU2MzU5IC0wLjI3OTI5NywtMy43NTc4MTIgMCwtMS4zMDkyNjggMC4xMjY0ODYsLTIuNjY3NDkyIDAuMzMwMDc4LC00LjA5MTc5NyBsIDAuMzQzNzUsLTIuNDEyMTA5IC05LjkxMjEwOSwtOC42OTE0MDcgNS45NjI4OSwtMTAuNTUwNzgxIDEyLjg3MzA0Nyw0LjEyNjk1MyAxLjg2NTIzNSwtMS40MTc5NjggYyAyLjA3MDMxMywtMS41NzMxMTQgNC4yOTE2OTMsLTIuODU2ODM5IDYuNjI4OTA2LC0zLjgwMjczNSBMIDQxLjMxNDQ1MywyMi4yODEyNSA0NC4zNzMwNDcsOS4xNzc3MzQ0IFogTSA1MCwzMS4xNTgyMDMgQyAzOS42NTEzNzgsMzEuMTU5MDEgMzEuMTcwNzI2LDM5LjY1MjQwMSAzMS4xNjk5MjIsNTAuMDAxOTUzIDMxLjE2OTMwNiw2MC4zNTI1NTkgMzkuNjUxMjM2LDY4Ljg0NDc1NiA1MCw2OC44NDU3MDMgNjAuMzQ5Nzg3LDY4Ljg0NjE3NiA2OC44MzI3MTksNjAuMzU0MTQ5IDY4LjgzMjAzMSw1MC4wMDE5NTMgNjguODMxMjI4LDM5LjY1MDc3MiA2MC4zNDk2MjQsMzEuMTU3NTE4IDUwLDMxLjE1ODIwMyBaIG0gMCw5IGMgNS40ODAxODIsLTMuNjNlLTQgOS44MzE2MDUsNC4zNTI5NjMgOS44MzIwMzEsOS44NDM3NSAzLjY1ZS00LDUuNDkxMzMzIC00LjM1MDM4NCw5Ljg0NDAwMSAtOS44MzIwMzEsOS44NDM3NSAtNS40ODEwNzQsLTUuMDFlLTQgLTkuODMwNDA1LC00LjM1MjI3OCAtOS44MzAwNzgsLTkuODQzNzUgQyA0MC4xNzAzNDgsNDQuNTExMTY5IDQ0LjUyMDM0LDQwLjE1ODYzIDUwLDQwLjE1ODIwMyBaXFxcIiBjb2xvcj1cXFwiIzAwMFxcXCIgZm9udC13ZWlnaHQ9XFxcIjQwMFxcXCIgZm9udC1mYW1pbHk9XFxcInNhbnMtc2VyaWZcXFwiIG92ZXJmbG93PVxcXCJ2aXNpYmxlXFxcIiBzdHJva2Utd2lkdGg9XFxcIjlcXFwiLz48L3N5bWJvbD5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvZ1N0cm9rZTtcclxuIiwidmFyIGFyZWFUb2dnbGVTdHJva2UgPSBcIjxzeW1ib2wgaWQ9XFxcImVnbWwtaWNvbnMtYXJlYS10b2dnbGUtc3Ryb2tlXFxcIiB2aWV3Qm94PVxcXCIwIDAgMjAgMjBcXFwiPjxwYXRoIGQ9XFxcIk0gMS40MTEsMTkuOTk5IDAsMTguNTg4IGwgMi4wNDQsLTIuMDQ0IDAsLTE0LjU0OSAxNC41NSwwIEwgMTguNTksMCAyMCwxLjQxMSBaIE0gNC4wNCwzLjk5IDQuMDQsMTQuNTQ5IDE0LjYsMy45OSBaIG0gNy45ODEsMTMuOTY0IC0zLjk5MiwwIDAsLTEuOTk0IDMuOTkxLDAgMCwxLjk5NCB6IG0gMy45ODksLTkuOTc0IDEuOTk1LDAgMCwzLjk5IC0xLjk5NSwwIHogbSAwLDUuOTg1IDEuOTk1LDAgMCwzLjk4OSAtMy45ODksMCAwLC0xLjk5NCAxLjk5NCwwIHpcXFwiIGNsaXAtcnVsZT1cXFwiZXZlbm9kZFxcXCIgZmlsbC1ydWxlPVxcXCJldmVub2RkXFxcIi8+PC9zeW1ib2w+XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBhcmVhVG9nZ2xlU3Ryb2tlO1xyXG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBJZCgpIHtcclxufVxyXG5JZC5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcy5jdXJyZW50O1xyXG59OyIsImltcG9ydCB7IGlzT2JqZWN0LCBleHRlbmQsIGZpcnN0VXBwZXJDYXNlIH0gZnJvbSAnLi91dGlscyc7XHJcbmltcG9ydCBOYW1lIGZyb20gJy4vTmFtZSc7XHJcbmltcG9ydCBJZCBmcm9tICcuL0lkJztcclxuXHJcbi8qKlxyXG4gKiDQkdCw0LfQvtCy0YvQuSDQutC70LDRgdGBINC00LvRjyDQstC40LTQttC10YLQvtCyXHJcbiAqIEBwYXJhbSB7QXJyYXktbGlrZX0gYXJndW1lbnRzIC4uLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gV2lkZ2V0KCkge1xyXG5cdHRoaXMuY29uc3RydWN0KC4uLmFyZ3VtZW50cyk7XHJcbn07XHJcblxyXG4vKipcclxuICog0J/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNC1INGB0LLQvtC50YHRgtCy0LAgYG5hbWVgINC60L7QvdGB0YLRgNGD0LrRgtC+0YDQsCDQvdGD0LbQvdC+INC/0YDQuCDRgdC+0YXRgNCw0L3QtdC90LjQuCDRjdC60LfQtdC/0LvRj9GA0LAg0LrQu9Cw0YHRgdCwINCyINC/0YDQvtGC0L7RgtC40L/QtVxyXG4gKiAo0YHQvC4g0LzQtdGC0L7QtCBgc2F2ZUluc3RhbmNlT2ZgKVxyXG4gKi9cclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KFdpZGdldCwgJ25hbWUnLCB7XHJcblx0dmFsdWU6IG5ldyBOYW1lKCdXaWRnZXQnLCBmYWxzZSksXHJcbn0pO1xyXG5cclxuV2lkZ2V0LnByb3RvdHlwZS5uYW1lID0gbmV3IE5hbWUoJ3dpZGdldCcpO1xyXG5XaWRnZXQucHJvdG90eXBlLmRlYnVnID0gZmFsc2U7XHJcbldpZGdldC5wcm90b3R5cGUuYnVzeSA9IGZhbHNlO1xyXG5XaWRnZXQucHJvdG90eXBlLmluaXRpYWxpemVkID0gZmFsc2U7XHJcbldpZGdldC5wcm90b3R5cGUuYXV0b0luaXRpYWxpemUgPSB0cnVlO1xyXG5cclxuLyoqXHJcbiAqINCa0L7QvdGB0YLRgNGD0LjRgNC+0LLQsNC90LjQtSDQvtCx0YrQtdC60YLQsC5cclxuICog0K3RgtC+0YIg0LzQtdGC0L7QtCDQvdC1INC30LDQtNGD0LzQsNC9INC00LvRjyDQv9C10YDQtdC+0L/RgNC10LTQtdC70LXQvdC40Y8g0LjQu9C4INGA0LDRgdGI0LjRgNC10L3QuNGPICjQtNC70Y8g0Y3RgtC+0LPQviDRgdC8LiBgY29uc3RydWN0aW9uQ2hhaW5gKS5cclxuICogQHBhcmFtIHtBcnJheS1saWtlfSBhcmd1bWVudHMgLi4uXHJcbiAqL1xyXG5XaWRnZXQucHJvdG90eXBlLmNvbnN0cnVjdCA9IGZ1bmN0aW9uKClcclxue1xyXG5cdHZhciB0YXJnZXQsIG9wdGlvbnM7XHJcblx0aWYgKGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIEVsZW1lbnQpIHtcclxuXHRcdHRhcmdldCA9IGFyZ3VtZW50c1swXTtcclxuXHRcdGlmICghIWFyZ3VtZW50c1sxXSAmJiB0eXBlb2YgYXJndW1lbnRzWzFdID09ICdvYmplY3QnKSB7XHJcblx0XHRcdG9wdGlvbnMgPSBhcmd1bWVudHNbMV07XHJcblx0XHRcdGlmICh0eXBlb2YgYXJndW1lbnRzWzJdID09ICdib29sZWFuJykge1xyXG5cdFx0XHRcdG9wdGlvbnMuYXV0b0luaXRpYWxpemUgPSBhcmd1bWVudHNbMl07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9IGVsc2UgaWYgKCEhYXJndW1lbnRzWzBdICYmIHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ29iamVjdCcpIHtcclxuXHRcdG9wdGlvbnMgPSBhcmd1bWVudHNbMF07XHJcblx0XHRpZiAodHlwZW9mIGFyZ3VtZW50c1sxXSA9PSAnYm9vbGVhbicpIHtcclxuXHRcdFx0b3B0aW9ucy5hdXRvSW5pdGlhbGl6ZSA9IGFyZ3VtZW50c1sxXTtcclxuXHRcdH1cclxuXHR9XHJcblx0dGhpcy5jb25zdHJ1Y3Rpb25DaGFpbih0YXJnZXQsIG9wdGlvbnMpO1xyXG5cdGlmICh0aGlzLmF1dG9Jbml0aWFsaXplKSB7XHJcblx0XHR0aGlzLmluaXRpYWxpemUodGhpcy5kb20uc2VsZik7XHJcblx0fVxyXG59O1xyXG5cclxuLyoqXHJcbiAqINCa0L7QvdGB0YLRgNGD0LjRgNC+0LLQsNC90LjQtSDQvtCx0YrQtdC60YLQsCAo0YLQvtC70YzQutC+INC+0YLQvdC+0YHRj9GJ0LXQtdGB0Y8g0Log0Y3RgtC+0Lwg0L7QsdGK0LXQutGC0YMgKyDQstGL0LfQvtCyINGG0LXQv9C+0YfQutC4KS4g0KLRg9GCINGC0L7Qu9GM0LrQviDRgtC+0YIg0LrQvtC0LCBcclxuICog0LrQvtGC0L7RgNGL0Lkg0L3QtdC70YzQt9GPINC90LDQt9Cy0LDRgtGMINC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC10LkgKNC60L7RgtC+0YDRi9C5INC90LUg0LTQvtC70LbQtdC9INC40YHQv9C+0LvQvdGP0YLRjNGB0Y8g0L/QvtCy0YLQvtGA0L3QvikuINCt0YLQvtGCINC80LXRgtC+0LQg0LfQsNC00YPQvNCw0L0gXHJcbiAqINC00LvRjyDRgNCw0YHRiNC40YDQtdC90LjRjyDQuNC70Lgg0L/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNGPINC00L7Rh9C10YDQvdC40LzQuCDQutC70LDRgdGB0LDQvNC4LlxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSB0YXJnZXQg0K3Qu9C10LzQtdC90YIgRE9NLCDQvdCwINC60L7RgtC+0YDQvtC8INGB0L7Qt9C00LDQtdGC0YHRjyDQstC40LTQttC10YIgKNC40L3QvtCz0LTQsCDQvtC9INC90YPQttC10L0g0LIg0LrQvtC90YHRgtGA0YPQutGC0L7RgNC1KVxyXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyDQn9Cw0YDQsNC80LXRgtGA0Ysg0LTQu9GPINGN0LrQt9C10LzQv9C70Y/RgNCwINC60LvQsNGB0YHQsCAo0L/QtdGA0LXQt9Cw0L/QuNGB0YvQstCw0LXRgiDQt9C90LDRh9C10L3QuNGPINC/0L4g0YPQvNC+0LvRh9Cw0L3QuNGOKVxyXG4gKi9cclxuV2lkZ2V0LnByb3RvdHlwZS5jb25zdHJ1Y3Rpb25DaGFpbiA9IGZ1bmN0aW9uKHRhcmdldCwgb3B0aW9ucylcclxue1xyXG5cdHRoaXMuZG9tID0ge1xyXG5cdFx0c2VsZjogdGFyZ2V0LFxyXG5cdH07XHJcblx0dGhpcy5pZCA9IG5ldyBJZDtcclxuXHR0aGlzLnNldE9wdGlvbnMob3B0aW9ucyk7XHJcblx0dGhpcy5zYXZlSW5zdGFuY2VPZihXaWRnZXQpO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqINCY0L3QuNGG0LjQsNC70LjQt9Cw0YbQuNGPINGN0LrQt9C10L/Qu9GP0YDQsCDQutC70LDRgdGB0LA6INC/0YDQuNCy0Y/Qt9C60LAg0LogRE9NLCDQvdCw0LfQvdCw0YfQtdC90LjQtSDQv9GA0L7RgdC70YPRiNC60Lgg0YHQvtCx0YvRgtC40Lkg0Lgg0YIu0L8uXHJcbiAqINCt0YLQvtGCINC80LXRgtC+0LQg0L3QtSDQt9Cw0LTRg9C80LDQvSDQtNC70Y8g0L/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNGPINC40LvQuCDRgNCw0YHRiNC40YDQtdC90LjRjyAo0LTQu9GPINGN0YLQvtCz0L4g0YHQvC4gYGluaXRpYWxpemF0aW9uQ2hhaW5gKVxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fG51bGx9IHRhcmdldCDQrdC70LXQvNC10L3RgiBET00sINC60L7RgtC+0YDRi9C5INCx0YPQtNC10YIg0L7RgdC90L7QstC90YvQvCDQv9GA0Lgg0YDQsNCx0L7RgtC1INCy0LjQtNC20LXRgtCwXHJcbiAqL1xyXG5XaWRnZXQucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbih0YXJnZXQpXHJcbntcclxuXHRpZiAodHlwZW9mIHRhcmdldCA9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0dGFyZ2V0ID0gdGhpcy5kb20uc2VsZjsgLy8g0J/RgNC4INC+0YLQu9C+0LbQtdC90L3QvtC5INC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC4XHJcblx0fVxyXG5cdHRoaXMuaW5pdGlhbGl6YXRpb25DaGFpbih0YXJnZXQpO1xyXG5cdHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqINCt0YLQviDQv9GB0LXQstC00L7QvdC40Lwg0LTQu9GPIGBpbml0aWFsaXplYC5cclxuICogQHBhcmFtIHtIVE1MRWxlbWVudHxudWxsfSB0YXJnZXQg0K3Qu9C10LzQtdC90YIgRE9NLCDQutC+0YLQvtGA0YvQuSDQsdGD0LTQtdGCINC+0YHQvdC+0LLQvdGL0Lwg0L/RgNC4INGA0LDQsdC+0YLQtSDQstC40LTQttC10YLQsFxyXG4gKi9cclxuV2lkZ2V0LnByb3RvdHlwZS5tb3VudCA9IGZ1bmN0aW9uKHRhcmdldClcclxue1xyXG5cdHRoaXMuaW5pdGlhbGl6ZSh0YXJnZXQpO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqINCY0L3QuNGG0LjQsNC70LjQt9Cw0YbQuNGPINGN0LrQt9C10L/Qu9GP0YDQsCDQutC70LDRgdGB0LAgKNGC0L7Qu9GM0LrQviDQvtGC0L3QvtGB0Y/RidCw0Y/RgdGPINC6INGN0YLQvtC80YMg0L7QsdGK0LXQutGC0YMgKyDQstGL0LfQvtCyINGG0LXQv9C+0YfQutC4KTogXHJcbiAqINC/0YDQuNCy0Y/Qt9C60LAg0LogRE9NLCDQvdCw0LfQvdCw0YfQtdC90LjQtSDQv9GA0L7RgdC70YPRiNC60Lgg0YHQvtCx0YvRgtC40Lkg0Lgg0YIu0L8uINCt0YLQvtGCINC80LXRgtC+0LQg0LfQsNC00YPQvNCw0L0g0LTQu9GPINGA0LDRgdGI0LjRgNC10L3QuNGPIFxyXG4gKiDQuNC70Lgg0L/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNGPINC00L7Rh9C10YDQvdC40LzQuCDQutC70LDRgdGB0LDQvNC4LlxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSB0YXJnZXQg0K3Qu9C10LzQtdC90YIgRE9NLCDQvdCwINC60L7RgtC+0YDQvtC8INGB0L7Qt9C00LDQtdGC0YHRjyDQstC40LTQttC10YJcclxuICovXHJcbldpZGdldC5wcm90b3R5cGUuaW5pdGlhbGl6YXRpb25DaGFpbiA9IGZ1bmN0aW9uKHRhcmdldClcclxue1xyXG5cdHRoaXMuZG9tLnNlbGYgPSB0YXJnZXQ7XHJcbn07XHJcblxyXG5XaWRnZXQucHJvdG90eXBlLnNldE9wdGlvbnMgPSBmdW5jdGlvbihvcHRpb25zKVxyXG57XHJcblx0aWYgKGlzT2JqZWN0KG9wdGlvbnMpKSB7XHJcblx0XHRleHRlbmQodGhpcywgb3B0aW9ucyk7XHJcblx0fVxyXG59O1xyXG5cclxuLy8gLypcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggICDilojilojilojiloggICDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIXHJcbi8vICovXHJcbi8vICNzYXZlICNpbnN0YW5jZSAjb2ZcclxuLyoqXHJcbiAqINCh0L7RhdGA0LDQvdC10L3QuNC1INGN0LrQt9C10LzQv9C70Y/RgNCwINC60LvQsNGB0YHQsCDQsiDRgNC10LPQuNGB0YLRgNC1INGN0LrQt9C10LzQv9C70Y/RgNC+0LIg0LrQu9Cw0YHRgdCwINGN0YLQvtCz0L4g0YLQuNC/0LBcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gY29uc3RydWN0b3Ig0JrQvtC90YHRgtGA0YPQutGC0L7RgCDQutC70LDRgdGB0LAsINCyINC60L7RgtC+0YDRi9C5INC/0YDQvtC40YHRhdC+0LTQuNGCINGB0L7RhdGA0LDQvdC10L3QuNC1INGN0LrQt9C10LzQv9C70Y/RgNCwXHJcbiAqL1xyXG5XaWRnZXQucHJvdG90eXBlLnNhdmVJbnN0YW5jZU9mID0gZnVuY3Rpb24oY29uc3RydWN0b3IpXHJcbntcclxuXHRjb25zdHJ1Y3RvciA9IGNvbnN0cnVjdG9yIHx8IHRoaXMuY29uc3RydWN0b3I7XHJcblx0aWYgKCFjb25zdHJ1Y3Rvci5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkoJ2luc3RhbmNlcycpKSB7XHJcblx0XHQoY29uc3RydWN0b3IucHJvdG90eXBlLmluc3RhbmNlcyA9IFtdKS5sZW5ndGgrKzsgLy8g0L/QvtGC0L7QvNGDINGH0YLQviDQvNGLINGF0L7RgtC40Lwg0YHRh9C40YLQsNGC0YwgaWQg0YEgMSwg0LAg0L3QtSDRgSAwXHJcblx0XHQvLyAjVE9ETzog0J/QtdGA0LXQtNC10LvQsNGC0Ywg0L3QsCDRh9GC0L4t0YLQviDQsdC+0LvQtdC1INC60L7RgNGA0LXQutGC0L3QvtC1LCDQstC10LTRjCDQv9GA0Lgg0YLQsNC60L7QvCDQv9C+0LTRhdC+0LTQtSDRgSDQvNCw0YHRgdC40LLQvtC8INC10LPQviBsZW5ndGgg0LHRg9C00LXRgiDQstGL0LTQsNCy0LDRgtGMINGA0LXQt9GD0LvRjNGC0LDRgiDQvdC1INC10LTQuNC90LjRhtGDINCx0L7Qu9GM0YjQuNC5LCDRh9C10Lwg0LIg0LTQtdC50YHRgtCy0LjRgtC10LvRjNC90L7RgdGC0Lgg0LXRgdGC0Ywg0YHQvtGF0YDQsNC90LXQvdC90YvRhSDRjdC60LfQtdC/0LvRj9GA0L7Qsi4g0JLQvtC30LzQvtC20L3QviDQu9GD0YfRiNC1INC40YHQv9C+0LvRjNC30L7QstCw0YLRjCBTZXQg0LjQu9C4IE1hcC5cclxuXHR9XHJcblx0dGhpcy5pZC5jdXJyZW50ID0gY29uc3RydWN0b3IucHJvdG90eXBlLmluc3RhbmNlcy5sZW5ndGg7XHJcblx0dGhpcy5pZFtjb25zdHJ1Y3Rvci5uYW1lLl9jYW1lbF0gPSB0aGlzLmlkID4+IDA7IC8vINGB0LrQsNGB0YLQvtCy0LDRgtGMINCyINGB0YLRgNC+0LrRgywg0LAg0LfQsNGC0LXQvCDQsiDRhtC10LvQvtC1INGH0LjRgdC70L5cclxuXHRjb25zdHJ1Y3Rvci5wcm90b3R5cGUuaW5zdGFuY2VzW3RoaXMuaWRdID0gdGhpcztcclxufTtcclxuXHJcbi8vIC8qXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAgICAg4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojiloggICAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilohcclxuLy8gKi9cclxuLy8gI3NldCAjZGF0YXNldCAjaWQgI29mXHJcbldpZGdldC5wcm90b3R5cGUuc2V0RGF0YXNldElkT2YgPSBmdW5jdGlvbihjb25zdHJ1Y3Rvcilcclxue1xyXG5cdGNvbnN0cnVjdG9yID0gY29uc3RydWN0b3IgfHwgdGhpcy5jb25zdHJ1Y3RvcjtcclxuXHR2YXIgaWQgPSB0aGlzLmlkW2NvbnN0cnVjdG9yLm5hbWUuX2NhbWVsXTtcclxuXHR2YXIgbmFtZSA9IGNvbnN0cnVjdG9yLnByb3RvdHlwZS5uYW1lLmNhbWVsKCdpZCcpO1xyXG5cdHRoaXMuZG9tLnNlbGYuZGF0YXNldFtuYW1lXSA9IGlkO1xyXG59O1xyXG5cclxuLy8gLypcclxuLy8gIOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIICAg4paI4paI4paI4paIICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paIICAgIOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paIICDilojilojilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjaW5zdGFuY2UgI2Zyb20gI2RhdGFzZXRcclxuV2lkZ2V0LnByb3RvdHlwZS5pbnN0YW5jZUZyb21EYXRhc2V0ID0gZnVuY3Rpb24oZWxlbWVudCwgY29uc3RydWN0b3IpXHJcbntcclxuXHRyZXR1cm4gY29uc3RydWN0b3IucHJvdG90eXBlLmluc3RhbmNlc1tlbGVtZW50LmRhdGFzZXRbY29uc3RydWN0b3IucHJvdG90eXBlLm5hbWUuY2FtZWwoJ2lkJyldXTtcclxufTtcclxuXHJcbldpZGdldC5wcm90b3R5cGUuc2V0U3RhdGUgPSBmdW5jdGlvbihzdGF0ZSlcclxue1xyXG5cdHZhciBtZXRob2ROYW1lID0gJ3NldFN0YXRlJyArIGZpcnN0VXBwZXJDYXNlKHN0YXRlKTtcclxuXHRpZiAodHlwZW9mIHRoaXNbbWV0aG9kTmFtZV0gPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0dGhpc1ttZXRob2ROYW1lXSgpO1xyXG5cdH1cclxufTtcclxuXHJcbldpZGdldC5wcm90b3R5cGUuY2xlYXJTdGF0ZSA9IGZ1bmN0aW9uKHN0YXRlKVxyXG57XHJcblx0aWYgKHN0YXRlID09ICdhbGwnKSB7XHJcblx0XHQvLyDQvtCx0YXQvtC0INCy0YHQtdGFXHJcblx0XHRmb3IgKHZhciBwcm9wZXJ0eSBpbiB0aGlzKSB7XHJcblx0XHRcdGlmIChwcm9wZXJ0eS5tYXRjaCgvXmNsZWFyU3RhdGUoPyFBbGwpLisvKSkge1xyXG5cdFx0XHRcdHRoaXNbcHJvcGVydHldKCk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9IGVsc2Uge1xyXG5cdFx0aWYgKCFzdGF0ZSkge1xyXG5cdFx0XHRpZiAoIXRoaXMuc3RhdGUpIHJldHVybjtcclxuXHRcdFx0c3RhdGUgPSB0aGlzLnN0YXRlO1xyXG5cdFx0fVxyXG5cdFx0dmFyIG1ldGhvZE5hbWUgPSAnY2xlYXJTdGF0ZScgKyBmaXJzdFVwcGVyQ2FzZShzdGF0ZSk7XHJcblx0XHRpZiAodHlwZW9mIHRoaXNbbWV0aG9kTmFtZV0gPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHR0aGlzW21ldGhvZE5hbWVdKCk7XHJcblx0XHR9XHJcblx0fVxyXG59OyIsImltcG9ydCBOYW1lIGZyb20gJ0BlZ21sL3V0aWxzL05hbWUuanMnO1xyXG5pbXBvcnQgV2lkZ2V0IGZyb20gJ0BlZ21sL3V0aWxzL1dpZGdldC5qcyc7XHJcblxyXG52YXIgbmFtZSA9IG5ldyBOYW1lKCdwYW5lbCcpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUGFuZWwoKSB7XHJcblx0V2lkZ2V0LmNhbGwodGhpcywgLi4uYXJndW1lbnRzKTtcclxufVxyXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoUGFuZWwsICduYW1lJywge1xyXG5cdHZhbHVlOiBuZXcgTmFtZSgnUGFuZWwnLCBmYWxzZSksXHJcbn0pO1xyXG5cclxuUGFuZWwucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShXaWRnZXQucHJvdG90eXBlKTtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KFBhbmVsLnByb3RvdHlwZSwgJ2NvbnN0cnVjdG9yJywge1xyXG5cdHZhbHVlOiBQYW5lbCxcclxuXHRlbnVtZXJhYmxlOiBmYWxzZSxcclxuXHR3cml0YWJsZTogdHJ1ZSxcclxufSk7XHJcblxyXG5QYW5lbC5wcm90b3R5cGUubmFtZSA9IG5hbWU7XHJcblBhbmVsLnByb3RvdHlwZS5zaG93biA9IGZhbHNlO1xyXG5cclxuLy9cdCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHQg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdCNjb25zdHJ1Y3RcclxuUGFuZWwucHJvdG90eXBlLmNvbnN0cnVjdGlvbkNoYWluID0gZnVuY3Rpb24odGFyZ2V0LCBvcHRpb25zKVxyXG57XHJcblx0V2lkZ2V0LnByb3RvdHlwZS5jb25zdHJ1Y3Rpb25DaGFpbi5jYWxsKHRoaXMsIHRhcmdldCwgb3B0aW9ucyk7XHJcblx0dGhpcy5zYXZlSW5zdGFuY2VPZihQYW5lbCk7XHJcbn07XHJcblxyXG4vL1x04paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilojiloggIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paI4paIICAg4paI4paI4paI4paI4paIXHJcbi8vXHTilojilogg4paI4paIICDilojilogg4paI4paIIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggIOKWiOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy9cdCNpbml0aWFsaXplXHJcblBhbmVsLnByb3RvdHlwZS5pbml0aWFsaXphdGlvbkNoYWluID0gZnVuY3Rpb24odGFyZ2V0KVxyXG57XHJcblx0V2lkZ2V0LnByb3RvdHlwZS5pbml0aWFsaXphdGlvbkNoYWluLmNhbGwodGhpcywgdGFyZ2V0KTtcclxuXHRpZiAoIXRoaXMuZG9tLnNlbGYpIHtcclxuXHRcdHRocm93IG5ldyBFcnJvcihg0J3QtSDQvdCw0LnQtNC10L3RiyDRjdC70LXQvNC10L3RgtGLINCyIERPTSDQtNC70Y8g0LLQuNC00LbQtdGC0LAg0YLQuNC/0LAgXCIke3RoaXMuY29uc3RydWN0b3IubmFtZX1cIiDRgSBJRCBcIiR7dGhpcy5pZFt0aGlzLmNvbnN0cnVjdG9yLm5hbWUuY2FtZWwoKV19XCJgKTtcclxuXHR9XHJcblx0dGhpcy5zZXREYXRhc2V0SWRPZihQYW5lbCk7XHJcblx0XHJcblx0dGhpcy5kb20uc2VsZi5zdHlsZS5kaXNwbGF5ID0gbnVsbDtcclxuXHR0aGlzLnNob3coKTtcclxufTtcclxuXHJcblBhbmVsLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24oKVxyXG57XHJcblx0aWYgKHRoaXMuc2hvd24gPT0gZmFsc2UpIHtcclxuXHRcdHRoaXMuZG9tLnNlbGYuY2xhc3NMaXN0LnJlbW92ZSh0aGlzLm5hbWUuY3NzTW9kKCdoaWRkZW4nKSk7XHJcblx0XHR0aGlzLnNob3duID0gdHJ1ZTtcclxuXHR9XHJcbn07XHJcblxyXG5QYW5lbC5wcm90b3R5cGUuaGlkZSA9IGZ1bmN0aW9uKClcclxue1xyXG5cdGlmICh0aGlzLnNob3duID09IHRydWUpIHtcclxuXHRcdHRoaXMuZG9tLnNlbGYuY2xhc3NMaXN0LmFkZCh0aGlzLm5hbWUuY3NzTW9kKCdoaWRkZW4nKSk7XHJcblx0XHR0aGlzLnNob3duID0gZmFsc2U7XHJcblx0fVxyXG59O1xyXG5cclxuUGFuZWwucHJvdG90eXBlLnRvZ2dsZSA9IGZ1bmN0aW9uKClcclxue1xyXG5cdGlmICh0aGlzLnNob3duID09IHRydWUpIHtcclxuXHRcdHRoaXMuaGlkZSgpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHR0aGlzLnNob3coKTtcclxuXHR9XHJcblx0cmV0dXJuIHRoaXMuc2hvd247XHJcbn07IiwiaW1wb3J0IE5hbWUgZnJvbSAnQGVnbWwvdXRpbHMvTmFtZS5qcyc7XHJcbmltcG9ydCB7IGluc2VydEh0bWwsIGluc2VydFN2Z1N5bWJvbCB9IGZyb20gJ0BlZ21sL3V0aWxzJztcclxuLy8gaW1wb3J0IG1haW5UZW1wbGF0ZSBmcm9tICcuLi9kaXN0L3BhbmVsLm11c3RhY2hlJztcclxuaW1wb3J0ICcuL21haW4uc2Nzcyc7XHJcbmltcG9ydCBpY29uQ3Jvc3MgZnJvbSAnQGVnbWwvaWNvbnMvZGlzdC9lc20vY3Jvc3Mtc3Ryb2tlLmpzJztcclxuaW1wb3J0IGljb25Db2cgZnJvbSAnQGVnbWwvaWNvbnMvZGlzdC9lc20vY29nLXN0cm9rZS5qcyc7XHJcbmltcG9ydCBpY29uQXJlYVRvZ2dsZSBmcm9tICdAZWdtbC9pY29ucy9kaXN0L2VzbS9hcmVhLXRvZ2dsZS1zdHJva2UuanMnO1xyXG5cclxuZXhwb3J0IHsgZGVmYXVsdCB9IGZyb20gJy4uL2Rpc3QvcGFuZWwuanMnO1xyXG5cclxudmFyIG5hbWUgPSBuZXcgTmFtZSgncGFuZWwnKTtcclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbigpIHtcclxuXHJcblx0aW5zZXJ0U3ZnU3ltYm9sKGljb25Db2cpO1xyXG5cdGluc2VydFN2Z1N5bWJvbChpY29uQ3Jvc3MpO1xyXG5cdGluc2VydFN2Z1N5bWJvbChpY29uQXJlYVRvZ2dsZSk7XHJcblxyXG5cdC8vIC8vINCa0LDQuiDQsdGD0LTRgtC+INGN0YLQviDQstC+0LfQstGA0LDRidCw0LXRgtGB0Y8g0L7RgiDRgdC10YDQstC10YDQsFxyXG5cdC8vIGluc2VydEh0bWwobWFpblRlbXBsYXRlLnJlbmRlcih7XHJcblx0Ly8gXHRpZDogMSxcclxuXHQvLyBcdG5hbWU6IG5hbWUuY3NzKCksXHJcblx0Ly8gXHRzY2FsZTogMSwgLy8gI1RPRE8oMSlcclxuXHQvLyBcdGRvY2thYmxlOiBmYWxzZSxcclxuXHQvLyB9KSk7XHJcblxyXG59KTsiXSwibmFtZXMiOlsidXRpbHMuc3RyaW5nQ2FzZSIsIm5hbWUiLCJpY29uQ29nIiwiaWNvbkNyb3NzIiwiaWNvbkFyZWFUb2dnbGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQUFBO0FBQ0EsQUF3REE7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQU8sU0FBUyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRTtDQUNyQyxDQUFDLEtBQUssSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO0NBQzFCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztDQUM1QixFQUFFO0NBQ0YsQ0FBQyxPQUFPLElBQUksQ0FBQztDQUNiLENBQUM7QUFDRCxBQWtHQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7QUFDQSxDQUFPLFNBQVMsUUFBUSxDQUFDLEtBQUssRUFBRTtDQUNoQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQztDQUMvQyxDQUFDO0FBQ0QsQUE2SEE7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQU8sU0FBUyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRTtDQUN2QyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO0NBQzVCLEVBQUUsT0FBTyxFQUFFLENBQUM7Q0FDWixFQUFFO0NBQ0YsQ0FBQyxJQUFJLE9BQU8sSUFBSSxJQUFJLFFBQVEsRUFBRTtDQUM5QixFQUFFLElBQUksR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO0NBQ2xCLEVBQUU7Q0FDRixDQUFDLElBQUksTUFBTSxJQUFJLEVBQUUsQ0FBQztDQUNsQixDQUFDLFFBQVEsSUFBSTtDQUNiLEVBQUUsS0FBSyxPQUFPO0NBQ2QsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNqQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtDQUNoQixLQUFLLE1BQU0sSUFBSSxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDbkMsS0FBSyxNQUFNO0NBQ1gsS0FBSyxNQUFNLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQ25DLEtBQUs7Q0FDTCxJQUFJLENBQUMsQ0FBQztDQUNOLEdBQUcsTUFBTTs7Q0FFVCxFQUFFLEtBQUssY0FBYztDQUNyQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEVBQUU7Q0FDOUIsSUFBSSxNQUFNLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQ2xDLElBQUksQ0FBQyxDQUFDO0NBQ04sR0FBRyxNQUFNOztDQUVULEVBQUUsS0FBSyxNQUFNO0NBQ2IsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztDQUMxQixHQUFHLE1BQU07O0NBRVQsRUFBRSxLQUFLLE9BQU87Q0FDZCxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQzNCLEdBQUcsTUFBTTs7Q0FFVCxFQUFFLEtBQUssT0FBTztDQUNkLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDM0IsR0FBRyxNQUFNOztDQUVULEVBQUUsS0FBSyxLQUFLO0NBQ1osR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztDQUMzQixHQUFHLE1BQU07Q0FDVDtDQUNBLEVBQUUsS0FBSyxNQUFNLENBQUM7Q0FDZCxFQUFFO0NBQ0YsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztDQUMxQixHQUFHLE1BQU07Q0FDVCxFQUFFO0NBQ0YsQ0FBQyxPQUFPLE1BQU0sQ0FBQztDQUNmLENBQUM7O0NBRUQ7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7QUFDQSxDQUFPLFNBQVMsY0FBYyxDQUFDLE1BQU0sRUFBRTtDQUN2QyxDQUFDLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDbEQsQ0FBQyxBQUNNLFNBQVMsY0FBYyxDQUFDLE1BQU0sRUFBRTtDQUN2QyxDQUFDLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDbEQsQ0FBQyxBQW9TRDtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0FBQ0EsQ0FBTyxTQUFTLGVBQWUsR0FBRztDQUNsQztDQUNBO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxPQUFPLEdBQUc7Q0FDZixFQUFFLE1BQU0sRUFBRSxJQUFJO0NBQ2QsRUFBRSxZQUFZLEVBQUUsUUFBUSxDQUFDLElBQUk7Q0FDN0IsRUFBRSxjQUFjLEVBQUUsSUFBSTtDQUN0QixFQUFFLENBQUM7Q0FDSDtDQUNBLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLE1BQU0sRUFBRTtDQUN4RSxFQUFFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2hDLEVBQUUsT0FBTyxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQztDQUM5RCxFQUFFLE9BQU8sQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUM7Q0FDbEUsRUFBRSxNQUFNLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLEVBQUU7Q0FDL0QsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2hDLEVBQUUsTUFBTTtDQUNSLEVBQUUsT0FBTztDQUNULEVBQUU7O0NBRUYsQ0FBQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO0NBQzlCLENBQUMsSUFBSSxjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWU7Q0FDNUMsRUFBRSxDQUFDO0dBQ0EsR0FBRyxPQUFPLENBQUMsY0FBYyxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFOztHQUV0RSxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDWixDQUFDO0NBQ1QsRUFBRSxlQUFlO0NBQ2pCLEVBQUUsQ0FBQztDQUNILENBQUMsSUFBSSxVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQzVFLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7O0NBRTlDO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBOztDQUVBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7O0NBRUE7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7O0VBQUMsRENseEJjLFNBQVMsVUFBVSxDQUFDLElBQUksRUFBRTtDQUN6QyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDOztDQUVsQixDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtDQUMxQixFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Q0FDakMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNoQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDL0IsR0FBRyxDQUFDLENBQUM7Q0FDTCxFQUFFLE1BQU07Q0FDUixFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztDQUN4QixFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDNUIsRUFBRTs7Q0FFRixDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzlDLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7Q0FDNUQsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHQSxVQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztDQUM5QyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzlDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Q0FDNUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHQSxVQUFnQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztDQUMxQyxDQUFDO0NBQ0QsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVztDQUMzQyxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztDQUNsQixDQUFDOztHQUFDLEZDckJhLFNBQVMsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUU7Q0FDL0MsQ0FBQyxJQUFJLElBQUksQ0FBQzs7Q0FFVjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO0NBQ3JCLEVBQUUsSUFBSSxPQUFPLE1BQU0sSUFBSSxTQUFTLElBQUksTUFBTSxJQUFJLEtBQUssRUFBRTtDQUNyRCxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUM7Q0FDZixHQUFHO0NBQ0gsRUFBRSxJQUFJLE9BQU8sTUFBTSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO0NBQzFELEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFO0NBQ3JDLElBQUksS0FBSyxFQUFFLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztDQUNqQyxJQUFJLFFBQVEsRUFBRSxJQUFJO0NBQ2xCLElBQUksWUFBWSxFQUFFLElBQUk7Q0FDdEIsSUFBSSxVQUFVLEVBQUUsSUFBSTtDQUNwQixJQUFJLENBQUMsQ0FBQztDQUNOLEdBQUc7Q0FDSCxFQUFFOztDQUVGLENBQUMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQzs7Q0FFakQsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztDQUV0QztDQUNBLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7Q0FDaEUsQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtDQUN4QyxFQUFFLElBQUksS0FBSyxFQUFFO0NBQ2IsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHQSxVQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDL0YsR0FBRyxNQUFNO0NBQ1QsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDOUMsR0FBRztDQUNILEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxJQUFJLEVBQUUsT0FBTyxFQUFFO0NBQzVDLEdBQUcsT0FBTyxTQUFTLE1BQU0sRUFBRTtDQUMzQixJQUFJLElBQUksTUFBTSxFQUFFO0NBQ2hCLEtBQUssSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO0NBQ3pCLE1BQU0sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO0NBQ2pDLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUU7Q0FDdkMsUUFBUSxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO0NBQ3RDLFFBQVEsQ0FBQyxDQUFDO0NBQ1YsT0FBTztDQUNQLE1BQU07Q0FDTixLQUFLLE9BQU9BLFVBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFQSxVQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQzFGLEtBQUssTUFBTTtDQUNYLEtBQUssT0FBTyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDO0NBQ2hDLEtBQUs7Q0FDTCxJQUFJLENBQUM7Q0FDTCxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQ3JCLEVBQUU7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztDQUNoQyxDQUFDLElBQUksS0FBSyxFQUFFO0NBQ1osRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7Q0FDOUIsRUFBRTtDQUNGLENBQUMsSUFBSSxDQUFDLElBQUksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7Q0FDM0MsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLFNBQVMsTUFBTSxFQUFFO0NBQzdCLEVBQUUsSUFBSSxNQUFNLEVBQUU7Q0FDZCxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtDQUM5QixJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxFQUFFO0NBQ3BDLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztDQUNuQyxLQUFLLENBQUMsQ0FBQztDQUNQLElBQUk7Q0FDSixHQUFHLE9BQU9BLFVBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFQSxVQUFnQixDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQ2xGLEdBQUcsTUFBTTtDQUNULEdBQUcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0NBQ3BCLEdBQUc7Q0FDSCxFQUFFLENBQUM7O0NBRUg7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQzs7Q0FFdkI7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Q0FDNUIsQ0FBQyxJQUFJLEtBQUssRUFBRTtDQUNaLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzlCLEVBQUU7Q0FDRixDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzdDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxTQUFTLE1BQU0sRUFBRTtDQUM3QixFQUFFLElBQUksTUFBTSxFQUFFO0NBQ2QsR0FBRyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7Q0FDOUIsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNwQyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDbkMsS0FBSyxDQUFDLENBQUM7Q0FDUCxJQUFJO0NBQ0osR0FBRyxPQUFPQSxVQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRUEsVUFBZ0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztDQUNwRixHQUFHLE1BQU07Q0FDVCxHQUFHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztDQUNwQixHQUFHO0NBQ0gsRUFBRSxDQUFDOztDQUVIO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsZUFBZSxFQUFFO0NBQ2pELEVBQUUsSUFBSSxlQUFlLElBQUksSUFBSSxFQUFFO0NBQy9CLEdBQUcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxFQUFFO0NBQ3ZDLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUU7Q0FDN0MsS0FBSyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO0NBQzVDLEtBQUssQ0FBQyxDQUFDO0NBQ1AsSUFBSTtDQUNKLEdBQUcsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHQSxVQUFnQixDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsQ0FBQztDQUN6RSxHQUFHLE1BQU07Q0FDVCxHQUFHLE9BQU8sSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0NBQ3JCLEdBQUc7Q0FDSCxFQUFFLENBQUM7Q0FDSCxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQzs7Q0FFbkM7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLFNBQVMsRUFBRTtDQUM3QyxFQUFFLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDbkMsRUFBRSxDQUFDO0NBQ0gsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsU0FBUyxFQUFFO0NBQ3JDLEVBQUUsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDMUMsRUFBRSxDQUFDO0NBQ0gsQ0FBQztDQUNELElBQUksRUFBRSxDQUFDO0NBQ1AsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRTtDQUM1QyxDQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRTtDQUN0QixFQUFFLEVBQUUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztDQUM3QixFQUFFO0NBQ0YsQ0FBQyxHQUFHLEVBQUUsV0FBVztDQUNqQixFQUFFLE9BQU8sRUFBRSxDQUFDO0NBQ1osRUFBRTtDQUNGLENBQUMsQ0FBQyxDQUFDO0NBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVztDQUNyQyxDQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztDQUNuQixDQUFDOztHQUFDOztDQ3BMRixJQUFJLFdBQVcsR0FBRyxzV0FBc1csQ0FBQzs7Q0NBelgsSUFBSSxTQUFTLEdBQUcsbXdGQUFtd0YsQ0FBQzs7Q0NBcHhGLElBQUksZ0JBQWdCLEdBQUcsMllBQTJZLENBQUM7O0NDQXBaLFNBQVMsRUFBRSxHQUFHO0NBQzdCLENBQUM7Q0FDRCxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxXQUFXO0NBQ25DLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0NBQ3JCLENBQUM7O0dBQUMsRkNBRjtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQWUsU0FBUyxNQUFNLEdBQUc7Q0FDakMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUM7Q0FDOUIsQ0FBQyxBQUNEO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUU7Q0FDdEMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQztDQUNqQyxDQUFDLENBQUMsQ0FBQzs7Q0FFSCxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztDQUMzQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Q0FDL0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0NBQzlCLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztDQUNyQyxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7O0NBRXZDO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRztDQUM3QjtDQUNBLENBQUMsSUFBSSxNQUFNLEVBQUUsT0FBTyxDQUFDO0NBQ3JCLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFlBQVksT0FBTyxFQUFFO0NBQ3RDLEVBQUUsTUFBTSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUN4QixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLEVBQUU7Q0FDekQsR0FBRyxPQUFPLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQzFCLEdBQUcsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxTQUFTLEVBQUU7Q0FDekMsSUFBSSxPQUFPLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUMxQyxJQUFJO0NBQ0osR0FBRztDQUNILEVBQUUsTUFBTSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksUUFBUSxFQUFFO0NBQy9ELEVBQUUsT0FBTyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUN6QixFQUFFLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksU0FBUyxFQUFFO0NBQ3hDLEdBQUcsT0FBTyxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDekMsR0FBRztDQUNILEVBQUU7Q0FDRixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Q0FDekMsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Q0FDMUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Q0FDakMsRUFBRTtDQUNGLENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxNQUFNLEVBQUUsT0FBTztDQUM3RDtDQUNBLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRztDQUNaLEVBQUUsSUFBSSxFQUFFLE1BQU07Q0FDZCxFQUFFLENBQUM7Q0FDSCxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxFQUFFLENBQUM7Q0FDbEIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0NBQzFCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztDQUM3QixDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsTUFBTTtDQUM3QztDQUNBLENBQUMsSUFBSSxPQUFPLE1BQU0sSUFBSSxXQUFXLEVBQUU7Q0FDbkMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7Q0FDekIsRUFBRTtDQUNGLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0NBQ2xDLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Q0FDekIsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxNQUFNO0NBQ3hDO0NBQ0EsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0NBQ3pCLENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLFNBQVMsTUFBTTtDQUN0RDtDQUNBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO0NBQ3hCLENBQUMsQ0FBQzs7Q0FFRixNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxTQUFTLE9BQU87Q0FDOUM7Q0FDQSxDQUFDLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO0NBQ3hCLEVBQUUsTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztDQUN4QixFQUFFO0NBQ0YsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLFNBQVMsV0FBVztDQUN0RDtDQUNBLENBQUMsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDO0NBQy9DLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFO0NBQ3pELEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxFQUFFLEVBQUUsTUFBTSxFQUFFLENBQUM7Q0FDbEQ7Q0FDQSxFQUFFO0NBQ0YsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7Q0FDMUQsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7Q0FDakQsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO0NBQ2pELENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsTUFBTSxDQUFDLFNBQVMsQ0FBQyxjQUFjLEdBQUcsU0FBUyxXQUFXO0NBQ3REO0NBQ0EsQ0FBQyxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUM7Q0FDL0MsQ0FBQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Q0FDM0MsQ0FBQyxJQUFJLElBQUksR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Q0FDbkQsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0NBQ2xDLENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsTUFBTSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxTQUFTLE9BQU8sRUFBRSxXQUFXO0NBQ3BFO0NBQ0EsQ0FBQyxPQUFPLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUNqRyxDQUFDLENBQUM7O0NBRUYsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxLQUFLO0NBQzFDO0NBQ0EsQ0FBQyxJQUFJLFVBQVUsR0FBRyxVQUFVLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQ3JELENBQUMsSUFBSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxVQUFVLEVBQUU7Q0FDNUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztDQUNyQixFQUFFO0NBQ0YsQ0FBQyxDQUFDOztDQUVGLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsS0FBSztDQUM1QztDQUNBLENBQUMsSUFBSSxLQUFLLElBQUksS0FBSyxFQUFFO0NBQ3JCO0NBQ0EsRUFBRSxLQUFLLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtDQUM3QixHQUFHLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO0NBQy9DLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7Q0FDckIsSUFBSTtDQUNKLEdBQUc7Q0FDSCxFQUFFLE1BQU07Q0FDUixFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7Q0FDZCxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE9BQU87Q0FDM0IsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztDQUN0QixHQUFHO0NBQ0gsRUFBRSxJQUFJLFVBQVUsR0FBRyxZQUFZLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQ3hELEVBQUUsSUFBSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxVQUFVLEVBQUU7Q0FDN0MsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztDQUN0QixHQUFHO0NBQ0gsRUFBRTtDQUNGLENBQUM7O0dBQUMsRkNqT0YsSUFBSSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7O0FBRTdCLENBQWUsU0FBUyxLQUFLLEdBQUc7Q0FDaEMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLFNBQVMsQ0FBQyxDQUFDO0NBQ2pDLENBQUM7Q0FDRCxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUU7Q0FDckMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQztDQUNoQyxDQUFDLENBQUMsQ0FBQzs7Q0FFSCxLQUFLLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0NBQ2xELE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUU7Q0FDdEQsQ0FBQyxLQUFLLEVBQUUsS0FBSztDQUNiLENBQUMsVUFBVSxFQUFFLEtBQUs7Q0FDbEIsQ0FBQyxRQUFRLEVBQUUsSUFBSTtDQUNmLENBQUMsQ0FBQyxDQUFDOztDQUVILEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztDQUM1QixLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7O0NBRTlCO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsS0FBSyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLE1BQU0sRUFBRSxPQUFPO0NBQzVEO0NBQ0EsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQ2hFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztDQUM1QixDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxLQUFLLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLFNBQVMsTUFBTTtDQUNyRDtDQUNBLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0NBQ3pELENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFO0NBQ3JCLEVBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDLDRDQUE0QyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUM1SSxFQUFFO0NBQ0YsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzVCO0NBQ0EsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztDQUNwQyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztDQUNiLENBQUMsQ0FBQzs7Q0FFRixLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztDQUN2QjtDQUNBLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssRUFBRTtDQUMxQixFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztDQUM3RCxFQUFFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0NBQ3BCLEVBQUU7Q0FDRixDQUFDLENBQUM7O0NBRUYsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUc7Q0FDdkI7Q0FDQSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7Q0FDekIsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Q0FDMUQsRUFBRSxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztDQUNyQixFQUFFO0NBQ0YsQ0FBQyxDQUFDOztDQUVGLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHO0NBQ3pCO0NBQ0EsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO0NBQ3pCLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0NBQ2QsRUFBRSxNQUFNO0NBQ1IsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDZCxFQUFFO0NBQ0YsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7Q0FDbkIsQ0FBQzs7R0FBQyxGQ3BFRixJQUFJQyxNQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7O0NBRTdCLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxXQUFXOztDQUV6RCxDQUFDLGVBQWUsQ0FBQ0MsU0FBTyxDQUFDLENBQUM7Q0FDMUIsQ0FBQyxlQUFlLENBQUNDLFdBQVMsQ0FBQyxDQUFDO0NBQzVCLENBQUMsZUFBZSxDQUFDQyxnQkFBYyxDQUFDLENBQUM7O0NBRWpDO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBOztDQUVBLENBQUMsQ0FBQzs7Ozs7Ozs7In0=
