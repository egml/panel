import Name from '@egml/utils/Name.js';
import { insertHtml, insertSvgSymbol } from '@egml/utils';
// import mainTemplate from '../dist/panel.mustache';
import './main.scss';
import iconCross from '@egml/icons/dist/esm/cross-stroke.js';
import iconCog from '@egml/icons/dist/esm/cog-stroke.js';
import iconAreaToggle from '@egml/icons/dist/esm/area-toggle-stroke.js';

export { default } from '../dist/panel.js';

var name = new Name('panel');

document.addEventListener('DOMContentLoaded', function() {

	insertSvgSymbol(iconCog);
	insertSvgSymbol(iconCross);
	insertSvgSymbol(iconAreaToggle);

	// // Как будто это возвращается от сервера
	// insertHtml(mainTemplate.render({
	// 	id: 1,
	// 	name: name.css(),
	// 	scale: 1, // #TODO(1)
	// 	dockable: false,
	// }));

});